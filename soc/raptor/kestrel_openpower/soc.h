/*
 * Copyright (c) 2018 - 2019 Antmicro <www.antmicro.com>
 * Copyright (c) 2021 - 2024 Raptor Engineering, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __POWERPC_LITEX_KESTREL_SOC_H_
#define __POWERPC_LITEX_KESTREL_SOC_H_

#include <zephyr/devicetree.h>

#define LITEX_SUBREG_ADDR_STEP     32
#define LITEX_API_DATA_TYPE_SIZE   32
#define LITEX_SUBREG_SIZE          0x1
#define LITEX_SUBREG_SIZE_BIT      (LITEX_SUBREG_SIZE * 8)
#define LITEX_STRIDE               (LITEX_SUBREG_ADDR_STEP / LITEX_SUBREG_SIZE_BIT)
#define LITEX_SUBREG_MASK          ((0x1 << LITEX_SUBREG_SIZE_BIT) - 1)

#ifndef _ASMLANGUAGE
/* CSR access helpers */

static inline unsigned char litex_read8(unsigned long addr)
{
	return sys_read8(addr);
}

static inline unsigned short litex_read16(unsigned long addr)
{
	return (sys_read8(addr) << 8)
		| sys_read8(addr + 0x4);
}

static inline unsigned int litex_read32(unsigned long addr)
{
	return (sys_read8(addr) << 24)
		| (sys_read8(addr + 0x4) << 16)
		| (sys_read8(addr + 0x8) << 8)
		| sys_read8(addr + 0xc);
}

static inline uint64_t litex_read64(unsigned long addr)
{
	return (((uint64_t)sys_read8(addr)) << 56)
		| ((uint64_t)sys_read8(addr + 0x4) << 48)
		| ((uint64_t)sys_read8(addr + 0x8) << 40)
		| ((uint64_t)sys_read8(addr + 0xc) << 32)
		| ((uint64_t)sys_read8(addr + 0x10) << 24)
		| ((uint64_t)sys_read8(addr + 0x14) << 16)
		| ((uint64_t)sys_read8(addr + 0x18) << 8)
		| (uint64_t)sys_read8(addr + 0x1c);
}

static inline void litex_write8(unsigned char value, unsigned long addr)
{
	sys_write8(value, addr);
}

static inline void litex_write16(unsigned short value, unsigned long addr)
{
	sys_write8(value >> 8, addr);
	sys_write8(value, addr + 0x4);
}

static inline void litex_write32(unsigned int value, unsigned long addr)
{
	sys_write8(value >> 24, addr);
	sys_write8(value >> 16, addr + 0x4);
	sys_write8(value >> 8, addr + 0x8);
	sys_write8(value, addr + 0xC);
}

static inline void litex_write64(uint64_t value, unsigned long addr)
{
	litex_write32(value >> 32, addr);
	litex_write32(value & 0xffffffff, addr + 0x10);
}

static inline void litex_write(volatile uint32_t *reg, uint32_t reg_size, uint32_t val)
{
	uint32_t shifted_data, i, align_bits_required;
	volatile uint32_t *reg_addr;

	align_bits_required = LITEX_API_DATA_TYPE_SIZE - (((reg_size / LITEX_STRIDE) * LITEX_SUBREG_SIZE_BIT) % LITEX_API_DATA_TYPE_SIZE);
	for (i = 0; i < reg_size / LITEX_STRIDE; ++i) {
		shifted_data = (val >> (((reg_size - i - 1) *
					LITEX_SUBREG_SIZE_BIT) - align_bits_required)) & LITEX_SUBREG_MASK;
		reg_addr = ((volatile uint32_t *) reg) + i;
		*(reg_addr) = shifted_data;
	}
}

static inline uint32_t litex_read(volatile uint32_t *reg, uint32_t reg_size)
{
	uint32_t shifted_data, i, align_bits_required, result = 0;

	align_bits_required = LITEX_API_DATA_TYPE_SIZE - (((reg_size / LITEX_STRIDE) * LITEX_SUBREG_SIZE_BIT) % LITEX_API_DATA_TYPE_SIZE);
	for (i = 0; i < reg_size / LITEX_STRIDE; ++i) {
		shifted_data = (*(reg + i) & LITEX_SUBREG_MASK) << (((reg_size - i - 1) *
						LITEX_SUBREG_SIZE_BIT) - align_bits_required);
		result |= shifted_data;
	}

	return result;
}

#endif /* _ASMLANGUAGE */

#endif /* __POWERPC_LITEX_KESTREL_SOC_H_ */

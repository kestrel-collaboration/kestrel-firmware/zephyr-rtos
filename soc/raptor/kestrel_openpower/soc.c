/*
 * Copyright (c) 2021-2022 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/init.h>
#include <zephyr/device.h>
#include <zephyr/logging/log.h>

#define LOG_LEVEL CONFIG_SOC_LOG_LEVEL
LOG_MODULE_REGISTER(soc);

#ifdef CONFIG_OPENPOWER_LITEX_IRQ
extern void microwatt_irq_set_base(void *ivt_base);
#endif

// SoC base controller information
#define SOC_CTL_BASE		DT_REG_ADDR(DT_INST(0, litex_soc_controller))

// Overrides the weak PowerPC implementation
// Reboot SoC
void sys_arch_reboot(int type)
{
	// Shut down interrupts
	irq_lock();

	// Reset IRQ vector table base address
#ifdef CONFIG_OPENPOWER_LITEX_IRQ
	microwatt_irq_set_base(0x0);
#endif

	// Hard reboot
	*((uint8_t*)(SOC_CTL_BASE)) = 1;
}

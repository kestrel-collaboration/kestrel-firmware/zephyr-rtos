/*
 * Copyright (c) 2016 Jean-Paul Etienne <fractalclone@gmail.com>
 * Contributors: 2018 Antmicro <www.antmicro.com>
 * Contributors: 2021 - 2022 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file
 * @brief POWERPC specific kernel interface header
 * This header contains the POWERPC specific kernel interface.  It is
 * included by the generic kernel interface header (arch/cpu.h)
 */

#ifndef ZEPHYR_INCLUDE_ARCH_POWERPC_ARCH_H_
#define ZEPHYR_INCLUDE_ARCH_POWERPC_ARCH_H_

#include <zephyr/arch/powerpc64le/thread.h>
#include <zephyr/arch/powerpc64le/exception.h>
#include <zephyr/arch/common/sys_bitops.h>
#include <zephyr/arch/common/sys_io.h>
#include <zephyr/arch/common/ffs.h>
#if defined(CONFIG_USERSPACE)
#include <zephyr/arch/powerpc64le/syscall.h>
#endif /* CONFIG_USERSPACE */
#include <zephyr/irq.h>
#include <zephyr/sw_isr_table.h>
#include <soc.h>
#include <zephyr/devicetree.h>

/* MSR bits */
#define PPC_MSR_EE_SHIFT	15	// External interrupt enable bit

/* stacks, for POWERPC architecture stack should be 16byte-aligned */
#define ARCH_STACK_PTR_ALIGN  16

#ifdef CONFIG_64BIT
#define RV_OP_LOADREG ld
#define RV_OP_STOREREG std
#define RV_REGSIZE 8
#define RV_REGSHIFT 3
#else
#define RV_OP_LOADREG lw
#define RV_OP_STOREREG sw
#define RV_REGSIZE 4
#define RV_REGSHIFT 2
#endif

/*
 * The 64bit PowerPC ABIs allows up to 512 bytes to be written below
 * the stack pointer. Whenever we take an exception and reuse the
 * existing stack, we need to be careful not to touch this space.
 */
#define USER_REDZONE_SIZE    512

/* A minimal ppc64le stack frame */
#define STACK_FRAME_C_MINIMAL   64

/* Size of the arch_esf structure (main stack frame) */
#define STACK_FRAME_ESF_SIZE sizeof(struct arch_esf)

/* We need space reserved for the redzone + sizeof(arch_esf) + the base C frame */
#define ARCH_KERNEL_STACK_RESERVED (USER_REDZONE_SIZE + STACK_FRAME_ESF_SIZE + STACK_FRAME_C_MINIMAL)

#ifndef _ASMLANGUAGE
#include <zephyr/sys/util.h>

#ifdef __cplusplus
extern "C" {
#endif

#define STACK_ROUND_UP(x) ROUND_UP(x, ARCH_STACK_PTR_ALIGN)
#define STACK_ROUND_DOWN(x) ROUND_DOWN(x, ARCH_STACK_PTR_ALIGN)

/* macros convert value of its argument to a string */
#define DO_TOSTR(s) #s
#define TOSTR(s) DO_TOSTR(s)

/*
 * SOC-specific function to get the IRQ number generating the interrupt.
 * __soc_get_irq returns a bitfield of pending IRQs.
 */
extern uint32_t __soc_get_irq(void);

void arch_irq_enable(unsigned int irq);
void arch_irq_disable(unsigned int irq);
int arch_irq_is_enabled(unsigned int irq);
void xics_arch_irq_priority_set(unsigned int irq, unsigned int prio, uint32_t flags);
void z_irq_spurious(const void *unused);

extern uint64_t arch_lock;
extern unsigned int arch_lock_key;

static ALWAYS_INLINE void mtmsrd(uint64_t val)
{
	__asm__ volatile("mtmsrd %0" : : "r" (val) : "memory");
}

static ALWAYS_INLINE uint64_t mfmsr(void)
{
	uint64_t rval;
	__asm__ volatile("mfmsr %0" : "=r" (rval) : : "memory");
	return rval;
}

static inline uint64_t __cmpxchg64(uint64_t *mem, uint64_t old, uint64_t new)
{
	uint64_t prev;

	__asm__ volatile(
		"# __cmpxchg64		\n"
		"1:	ldarx	%0,0,%2	\n"
		"	cmpd	%0,%3	\n"
		"	bne-	2f	\n"
		"	stdcx.	%4,0,%2	\n"
		"	bne-	1b	\n"
		"2:			\n"

		: "=&r"(prev), "+m"(*mem)
		: "r"(mem), "r"(old), "r"(new)
		: "cr0");

	return prev;
}

/* FIXME need some memory barriers for SMP */
static inline void spin_lock(uint64_t *lock)
{
	while (1) {
		while (*lock) {}

		if (__cmpxchg64(lock, 0, 1) == 0)
			return;
	}
}

static inline void spin_unlock(uint64_t *lock)
{
	*lock = 0;
}

static ALWAYS_INLINE unsigned int arch_irq_lock(void)
{
	uint64_t msr;
	unsigned int key = 0;

        msr = mfmsr();
	if (msr & (1 << PPC_MSR_EE_SHIFT))
		key = 1; /* IRQs are on */

	/* Turn IRQs off */
	mtmsrd(mfmsr() & ~(1 << PPC_MSR_EE_SHIFT));
	return key;
}

static ALWAYS_INLINE void arch_irq_unlock(unsigned int key)
{

	if (key == 1)
		/* Turn IRQs on */
		mtmsrd(mfmsr() | (1 << PPC_MSR_EE_SHIFT));
}

static ALWAYS_INLINE bool arch_irq_unlocked(unsigned int key)
{
	return key;
}

static ALWAYS_INLINE void arch_nop(void)
{
	__asm__ volatile("nop");
}

extern uint32_t sys_clock_cycle_get_32(void);

static inline uint32_t arch_k_cycle_get_32(void)
{
	return sys_clock_cycle_get_32();
}

extern uint64_t sys_clock_cycle_get_64(void);

static inline uint64_t arch_k_cycle_get_64(void)
{
	return sys_clock_cycle_get_64();
}

#ifdef CONFIG_OPENPOWER_LITEX_IRQ
#define ARCH_IRQ_CONNECT(irq_p, priority_p, isr_p, isr_param_p, flags_p) \
({ \
	Z_ISR_DECLARE(irq_p, 0, isr_p, isr_param_p); \
	xics_arch_irq_priority_set(irq_p, priority_p, flags_p); \
})
#else
#define ARCH_IRQ_CONNECT(irq_p, priority_p, isr_p, isr_param_p, flags_p) \
({ \
	Z_ISR_DECLARE(irq_p, 0, isr_p, isr_param_p); \
	irq_p; \
})
#endif

// Debug and tracing functions
void z_powerpc_safe_print_string(char * string);
void z_powerpc_safe_print_register(uint64_t reg, char print_newline);
void z_powerpc_safe_print_backtrace(uint64_t esf, int is_raw_stack_frame);
void z_powerpc_safe_dump_memory_block(uint64_t base, int count);
FUNC_NORETURN void z_powerpc_unhandled_exception_handler(uint64_t vector, uint64_t esf, int is_raw_stack_frame);

#ifdef __cplusplus
}
#endif

#endif /*_ASMLANGUAGE */

#endif

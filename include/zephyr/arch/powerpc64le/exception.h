/*
 * Copyright (c) 2019 Michael Neuling <mikey@neuling.org>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file
 * @brief PowerPC public exception handling
 *
 * PowerPC-specific kernel exception handling interface.
 */

#ifndef ZEPHYR_INCLUDE_ARCH_POWERPC_EXCEPTION_H_
#define ZEPHYR_INCLUDE_ARCH_POWERPC_EXCEPTION_H_

#ifndef _ASMLANGUAGE
#include <zephyr/types.h>

#ifdef __cplusplus
extern "C" {
#endif

struct arch_esf {
	uint64_t backptr;
	uint64_t nia;
	uint64_t srr1;
	uint64_t lr;
	uint64_t ctr;
	uint64_t xer;
	uint64_t cr;

	uint64_t r0;
	uint64_t r1;
	uint64_t r2;
	uint64_t r3;
	uint64_t r4;
	uint64_t r5;
	uint64_t r6;
	uint64_t r7;
	uint64_t r8;
	uint64_t r9;
	uint64_t r10;
	uint64_t r11;
	uint64_t r12;
	uint64_t r13;
	uint64_t r14;
	uint64_t r15;
	uint64_t r16;
	uint64_t r17;
	uint64_t r18;
	uint64_t r19;
	uint64_t r20;
	uint64_t r21;
	uint64_t r22;
	uint64_t r23;
	uint64_t r24;
	uint64_t r25;
	uint64_t r26;
	uint64_t r27;
	uint64_t r28;
	uint64_t r29;
	uint64_t r30;
	uint64_t r31;
};

#ifdef __cplusplus
}
#endif

#endif /* _ASMLANGUAGE */

#endif /* ZEPHYR_INCLUDE_ARCH_POWERPC_EXCEPTION_H_ */

/*
 * Copyright (c) 2016 Jean-Paul Etienne <fractalclone@gmail.com>
 * Copyright (c) 2022 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/kernel_structs.h>
#include <inttypes.h>
#include <zephyr/logging/log.h>

#define UART_BASE_ADDR	0xc0002000
#define UART_RXTX	((UART_BASE_ADDR) + 0x00)
#define UART_TXFULL	((UART_BASE_ADDR) + 0x04)

#define STACK_POISON 0xBADC0FFEE0DDF00D

struct __powerpc_abi_v2_stack_frame {
	uint64_t backptr;
	uint64_t cr;
	uint64_t lr;
	uint64_t compiler_dword;
	uint64_t link_editor_dword;
	uint64_t toc_save;
	uint64_t r3;
	uint64_t r4;
	uint64_t r5;
	uint64_t r6;
	uint64_t r7;
	uint64_t r8;
	uint64_t r9;
	uint64_t r10;
};

typedef struct __powerpc_abi_v2_stack_frame __powerpc_abi_v2_stack_frame_t;

LOG_MODULE_DECLARE(os);

FUNC_NORETURN void z_powerpc_fatal_error(unsigned int reason,
				       const struct arch_esf *esf)
{
	LOG_ERR("z_powerpc_fatal_error");
	while (1);
}

__attribute__((always_inline)) inline static void z_powerpc_safe_print_char_litex_uart(unsigned char c)
{
	while (sys_read8(UART_TXFULL));

	sys_write8(c, UART_RXTX);
}

void z_powerpc_safe_print_string(char * string)
{
	int i;
	int len = strlen(string);

	for (i=0; i<len; i++) {
		z_powerpc_safe_print_char_litex_uart(*(string + i));
	}
}

void z_powerpc_safe_print_register(uint64_t reg, char print_newline)
{
	int i;
	uint8_t byte;

	z_powerpc_safe_print_char_litex_uart('0');
	z_powerpc_safe_print_char_litex_uart('x');
	for (i=0; i<16; i++) {
		byte = (reg >> ((15 - i) * 4)) & 0xf;
		if (byte > 0x9) {
			z_powerpc_safe_print_char_litex_uart(byte + 0x37);
		}
		else {
			z_powerpc_safe_print_char_litex_uart(byte + 0x30);
		}
	}
	if (print_newline) {
		z_powerpc_safe_print_char_litex_uart('\n');
		z_powerpc_safe_print_char_litex_uart('\r');
	}
}

void z_powerpc_safe_print_stack_frame(const struct arch_esf *esf)
{
	z_powerpc_safe_print_string("===== STACK FRAME ");
	z_powerpc_safe_print_register((uint64_t)esf, 0);
	z_powerpc_safe_print_string(" =====\n\r");
	z_powerpc_safe_print_string("BKPTR:\t");
	z_powerpc_safe_print_register(esf->backptr, 1);
	z_powerpc_safe_print_string("NIA:\t");
	z_powerpc_safe_print_register(esf->nia, 1);
	z_powerpc_safe_print_string("SRR1:\t");
	z_powerpc_safe_print_register(esf->srr1, 1);
	z_powerpc_safe_print_string("LR:\t");
	z_powerpc_safe_print_register(esf->lr, 1);
	z_powerpc_safe_print_string("CTR:\t");
	z_powerpc_safe_print_register(esf->ctr, 1);
	z_powerpc_safe_print_string("CR:\t");
	z_powerpc_safe_print_register(esf->cr, 1);
	z_powerpc_safe_print_string("\n\r");
	z_powerpc_safe_print_string("R0:\t");
	z_powerpc_safe_print_register(esf->r0, 1);
	z_powerpc_safe_print_string("R1:\t");
	z_powerpc_safe_print_register(esf->r1, 1);
	z_powerpc_safe_print_string("R2:\t");
	z_powerpc_safe_print_register(esf->r2, 1);
	z_powerpc_safe_print_string("R3:\t");
	z_powerpc_safe_print_register(esf->r3, 1);
	z_powerpc_safe_print_string("R4:\t");
	z_powerpc_safe_print_register(esf->r4, 1);
	z_powerpc_safe_print_string("R5:\t");
	z_powerpc_safe_print_register(esf->r5, 1);
	z_powerpc_safe_print_string("R6:\t");
	z_powerpc_safe_print_register(esf->r6, 1);
	z_powerpc_safe_print_string("R7:\t");
	z_powerpc_safe_print_register(esf->r7, 1);
	z_powerpc_safe_print_string("R8:\t");
	z_powerpc_safe_print_register(esf->r8, 1);
	z_powerpc_safe_print_string("R9:\t");
	z_powerpc_safe_print_register(esf->r9, 1);
	z_powerpc_safe_print_string("R10:\t");
	z_powerpc_safe_print_register(esf->r10, 1);
	z_powerpc_safe_print_string("R11:\t");
	z_powerpc_safe_print_register(esf->r11, 1);
	z_powerpc_safe_print_string("R12:\t");
	z_powerpc_safe_print_register(esf->r12, 1);
	z_powerpc_safe_print_string("R13:\t");
	z_powerpc_safe_print_register(esf->r13, 1);
	z_powerpc_safe_print_string("R14:\t");
	z_powerpc_safe_print_register(esf->r14, 1);
	z_powerpc_safe_print_string("R15:\t");
	z_powerpc_safe_print_register(esf->r15, 1);
	z_powerpc_safe_print_string("R16:\t");
	z_powerpc_safe_print_register(esf->r16, 1);
	z_powerpc_safe_print_string("R17:\t");
	z_powerpc_safe_print_register(esf->r17, 1);
	z_powerpc_safe_print_string("R18:\t");
	z_powerpc_safe_print_register(esf->r18, 1);
	z_powerpc_safe_print_string("R19:\t");
	z_powerpc_safe_print_register(esf->r19, 1);
	z_powerpc_safe_print_string("R20:\t");
	z_powerpc_safe_print_register(esf->r20, 1);
	z_powerpc_safe_print_string("R21:\t");
	z_powerpc_safe_print_register(esf->r21, 1);
	z_powerpc_safe_print_string("R22:\t");
	z_powerpc_safe_print_register(esf->r22, 1);
	z_powerpc_safe_print_string("R23:\t");
	z_powerpc_safe_print_register(esf->r23, 1);
	z_powerpc_safe_print_string("R24:\t");
	z_powerpc_safe_print_register(esf->r24, 1);
	z_powerpc_safe_print_string("R25:\t");
	z_powerpc_safe_print_register(esf->r25, 1);
	z_powerpc_safe_print_string("R26:\t");
	z_powerpc_safe_print_register(esf->r26, 1);
	z_powerpc_safe_print_string("R27:\t");
	z_powerpc_safe_print_register(esf->r27, 1);
	z_powerpc_safe_print_string("R28:\t");
	z_powerpc_safe_print_register(esf->r28, 1);
	z_powerpc_safe_print_string("R29:\t");
	z_powerpc_safe_print_register(esf->r29, 1);
	z_powerpc_safe_print_string("R30:\t");
	z_powerpc_safe_print_register(esf->r30, 1);
	z_powerpc_safe_print_string("R31:\t");
	z_powerpc_safe_print_register(esf->r31, 1);
}

void z_powerpc_safe_print_raw_stack_frame(const __powerpc_abi_v2_stack_frame_t *esf)
{
	z_powerpc_safe_print_string("===== STACK FRAME ");
	z_powerpc_safe_print_register((uint64_t)esf, 0);
	z_powerpc_safe_print_string(" =====\n\r");
	z_powerpc_safe_print_string("BKPTR:\t");
	z_powerpc_safe_print_register(esf->backptr, 1);
	z_powerpc_safe_print_string("CR:\t");
	z_powerpc_safe_print_register(esf->cr, 1);
	z_powerpc_safe_print_string("LR:\t");
	z_powerpc_safe_print_register(esf->lr, 1);
	z_powerpc_safe_print_string("CDW:\t");
	z_powerpc_safe_print_register(esf->compiler_dword, 1);
	z_powerpc_safe_print_string("LEDW:\t");
	z_powerpc_safe_print_register(esf->link_editor_dword, 1);
	z_powerpc_safe_print_string("TOC:\t");
	z_powerpc_safe_print_register(esf->toc_save, 1);
	z_powerpc_safe_print_string("\n\r");
	z_powerpc_safe_print_string("R3:\t");
	z_powerpc_safe_print_register(esf->r3, 1);
	z_powerpc_safe_print_string("R4:\t");
	z_powerpc_safe_print_register(esf->r4, 1);
	z_powerpc_safe_print_string("R5:\t");
	z_powerpc_safe_print_register(esf->r5, 1);
	z_powerpc_safe_print_string("R6:\t");
	z_powerpc_safe_print_register(esf->r6, 1);
	z_powerpc_safe_print_string("R7:\t");
	z_powerpc_safe_print_register(esf->r7, 1);
	z_powerpc_safe_print_string("R8:\t");
	z_powerpc_safe_print_register(esf->r8, 1);
	z_powerpc_safe_print_string("R9:\t");
	z_powerpc_safe_print_register(esf->r9, 1);
	z_powerpc_safe_print_string("R10:\t");
	z_powerpc_safe_print_register(esf->r10, 1);
}

void z_powerpc_safe_print_backtrace(uint64_t esf, int is_raw_stack_frame)
{
	uint64_t backchain;

	// Print all stack frames
	backchain = esf;
	while (backchain
		&& (backchain != STACK_POISON)
		&& (backchain != 0xaaaaaaaaaaaaaaaa)
		&& (backchain != 0xffffffffffffffff)) {
		if (is_raw_stack_frame) {
			z_powerpc_safe_print_raw_stack_frame((__powerpc_abi_v2_stack_frame_t*)backchain);
		}
		else {
			z_powerpc_safe_print_stack_frame((struct arch_esf *)backchain);
			// Any additional frames are raw frames
			is_raw_stack_frame = 1;
		}
		z_powerpc_safe_print_string("\n\r");
		backchain = ((struct arch_esf *)backchain)->backptr;
	}
}

void z_powerpc_safe_dump_memory_block(uint64_t base, int count)
{
	int i;
	uint64_t addr;

	addr = base & 0xfffffffffffffff0;
	for (i=0; i<count; i++) {
		z_powerpc_safe_print_register((uint64_t)(addr + i), 0);
		z_powerpc_safe_print_string(":\t");
		z_powerpc_safe_print_register(*((uint64_t*)(addr + i)), 1);
	}
}

FUNC_NORETURN void z_powerpc_unhandled_exception_handler(uint64_t vector,
				       uint64_t esf, int is_raw_stack_frame)
{
	uint64_t link_register;
	__asm__ volatile ("mfspr %0, %1" : "=r" (link_register) : "n" (8));

	/* Turn IRQs off */
	mtmsrd(mfmsr() & ~(1 << PPC_MSR_EE_SHIFT));

	z_powerpc_safe_print_string("***** FATAL ERROR *****\n\r");
	z_powerpc_safe_print_string("VECTOR:\t");
	z_powerpc_safe_print_register(vector, 1);
	z_powerpc_safe_print_string("STACK:\t");
	z_powerpc_safe_print_register(esf, 1);

	// Link register at error handler entry
	z_powerpc_safe_print_string("\n\r");
	z_powerpc_safe_print_string("ELR:\t");
	z_powerpc_safe_print_register(link_register, 1);
	z_powerpc_safe_print_string("\n\r");

	// Print all stack frames
	z_powerpc_safe_print_backtrace(esf, is_raw_stack_frame);

	// Halt
	z_powerpc_safe_print_string("***** SYSTEM HALTED *****\n\r");
	while (1);
}

/*
 * Copyright (c) 2016 Jean-Paul Etienne <fractalclone@gmail.com>
 * Copyright (c) 2021-2022 Raptor Engineering, LLC <support@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/toolchain.h>
#include <zephyr/kernel_structs.h>
#include <zephyr/fatal.h>

#ifdef CONFIG_OPENPOWER_LITEX_IRQ
	extern void xics_arch_irq_enable(unsigned int irq);
	extern void xics_arch_irq_disable(unsigned int irq);
	extern int xics_arch_irq_is_enabled(unsigned int irq);
	extern void xics_arch_irq_priority_set(unsigned int irq, unsigned int prio, uint32_t flags);
#endif

void arch_irq_enable(unsigned int irq)
{
	unsigned int key = irq_lock();

#ifdef CONFIG_OPENPOWER_LITEX_IRQ
	xics_arch_irq_enable(irq);
#else
	/* FIXME: enable/disable these when we have an IRQ controller */
#endif

	irq_unlock(key);
}

void arch_irq_disable(unsigned int irq)
{
	unsigned int key = irq_lock();

#ifdef CONFIG_OPENPOWER_LITEX_IRQ
	xics_arch_irq_disable(irq);
#else
	/* FIXME: enable/disable these when we have an IRQ controller */
#endif

	irq_unlock(key);
}

int arch_irq_is_enabled(unsigned int irq)
{
#ifdef CONFIG_OPENPOWER_LITEX_IRQ
	return xics_arch_irq_is_enabled(irq);
#else
	/* FIXME: return active/inactive status when we have an IRQ controller */
	return 1;
#endif
}

void z_irq_priority_set(unsigned int irq, unsigned int prio, uint32_t flags)
{
	ARG_UNUSED(flags);


	unsigned int key = irq_lock();

#ifdef CONFIG_OPENPOWER_LITEX_IRQ
	xics_arch_irq_priority_set(irq, prio, flags);
#else
	/* FIXME: actually do something here */
#endif

	irq_unlock(key);
}

/*
 * @brief Spurious interrupt handler
 *
 * Installed in all dynamic interrupt slots at boot time. Throws an error if
 * called.
 *
 * @return N/A
 */

void z_irq_spurious(const void *unused)
{
	ARG_UNUSED(unused);
	z_fatal_error(K_ERR_SPURIOUS_IRQ, NULL);
}

/*
 * Copyright (c) 2019 Anton Blanchard <anton@linux.ibm.com>
 * Copyright (c) 2021-2022 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <ksched.h>
#include <zephyr/kernel_structs.h>
#include <offsets.h>

#define STACK_POISON 0xBADC0FFEE0DDF00D

void *z_arch_get_next_switch_handle(void * old_thread_stack)
{
	return z_get_next_switch_handle(old_thread_stack);
}

void arch_new_thread(struct k_thread *thread, k_thread_stack_t *stack,
		char *stack_ptr, k_thread_entry_t entry,
		void *p1, void *p2, void *p3)
{
	struct arch_esf *stack_init;
	char* currrent_stack_pointer;

	/*
	 * We need a minimal stack frame for the called function to write its
	 * LR into.
	 */
	currrent_stack_pointer = stack_ptr - STACK_FRAME_C_MINIMAL;

	/*
	 * Poison the back chain pointer and the LR so it's obvious if
	 * z_thread_entry() returns.
	 */
	*(unsigned long *)currrent_stack_pointer = STACK_POISON;

	/* Now create the stack frame that z_arch_switch will use. */
	currrent_stack_pointer -= sizeof(struct arch_esf) + USER_REDZONE_SIZE;

	stack_init = (struct arch_esf *)currrent_stack_pointer;
	memset(stack_init, 0, sizeof(struct arch_esf));
	stack_init->backptr = (uintptr_t)(currrent_stack_pointer + sizeof(struct arch_esf) + USER_REDZONE_SIZE);
	stack_init->nia = (uint64_t)z_thread_entry;
	stack_init->srr1 = (uint64_t)0x9000000000008003UL;
	stack_init->r3 = (uint64_t)entry;
	stack_init->r4 = (uint64_t)p1;
	stack_init->r5 = (uint64_t)p2;
	stack_init->r6 = (uint64_t)p3;

	/* Store the stack pointer away for z_arch_switch. */
	thread->switch_handle = (void *)currrent_stack_pointer;

	return;
}

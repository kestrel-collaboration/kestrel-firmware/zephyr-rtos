/*
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <kernel_arch_data.h>
#include <gen_offset.h>
#include <kernel_offsets.h>

GEN_OFFSET_STRUCT(arch_esf, nia);
GEN_OFFSET_STRUCT(arch_esf, srr1);
GEN_OFFSET_STRUCT(arch_esf, lr);
GEN_OFFSET_STRUCT(arch_esf, ctr);
GEN_OFFSET_STRUCT(arch_esf, xer);
GEN_OFFSET_STRUCT(arch_esf, cr);
GEN_OFFSET_STRUCT(arch_esf, r0);
GEN_OFFSET_STRUCT(arch_esf, r1);
GEN_OFFSET_STRUCT(arch_esf, r2);
GEN_OFFSET_STRUCT(arch_esf, r3);
GEN_OFFSET_STRUCT(arch_esf, r4);
GEN_OFFSET_STRUCT(arch_esf, r5);
GEN_OFFSET_STRUCT(arch_esf, r6);
GEN_OFFSET_STRUCT(arch_esf, r7);
GEN_OFFSET_STRUCT(arch_esf, r8);
GEN_OFFSET_STRUCT(arch_esf, r9);
GEN_OFFSET_STRUCT(arch_esf, r10);
GEN_OFFSET_STRUCT(arch_esf, r11);
GEN_OFFSET_STRUCT(arch_esf, r12);
GEN_OFFSET_STRUCT(arch_esf, r13);
GEN_OFFSET_STRUCT(arch_esf, r14);
GEN_OFFSET_STRUCT(arch_esf, r15);
GEN_OFFSET_STRUCT(arch_esf, r16);
GEN_OFFSET_STRUCT(arch_esf, r17);
GEN_OFFSET_STRUCT(arch_esf, r18);
GEN_OFFSET_STRUCT(arch_esf, r19);
GEN_OFFSET_STRUCT(arch_esf, r20);
GEN_OFFSET_STRUCT(arch_esf, r21);
GEN_OFFSET_STRUCT(arch_esf, r22);
GEN_OFFSET_STRUCT(arch_esf, r23);
GEN_OFFSET_STRUCT(arch_esf, r24);
GEN_OFFSET_STRUCT(arch_esf, r25);
GEN_OFFSET_STRUCT(arch_esf, r26);
GEN_OFFSET_STRUCT(arch_esf, r27);
GEN_OFFSET_STRUCT(arch_esf, r28);
GEN_OFFSET_STRUCT(arch_esf, r29);
GEN_OFFSET_STRUCT(arch_esf, r30);
GEN_OFFSET_STRUCT(arch_esf, r31);

GEN_ABSOLUTE_SYM(__struct_arch_esf_SIZEOF, STACK_ROUND_UP(sizeof(struct arch_esf)));

GEN_ABS_SYM_END

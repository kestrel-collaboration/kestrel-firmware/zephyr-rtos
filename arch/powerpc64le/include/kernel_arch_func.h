/*
 * Copyright (c) 2019-2020 Cobham Gaisler AB
 * Copyright (c) 2021 Raptor Engineering, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file
 * @brief Private kernel definitions
 *
 * This file contains private kernel function/macro definitions and various
 * other definitions for the PowerPC processor architecture.
 */

#ifndef ZEPHYR_ARCH_POWERPC_INCLUDE_KERNEL_ARCH_FUNC_H_
#define ZEPHYR_ARCH_POWERPC_INCLUDE_KERNEL_ARCH_FUNC_H_

#include <kernel_arch_data.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

static ALWAYS_INLINE void mtdec(uint64_t val)
{
	__asm__ volatile("mtdec %0" : : "r" (val) : "memory");
}

static ALWAYS_INLINE void arch_kernel_init(void)
{
	// Power PC is special in that the decrementer will start firing interrupts
	// as soon as MSR_EE is set to 1, unless the decrementer is preloaded prior
	// to setting MSR_EE 1.
	// Since Zephyr doesn't expect interrupts and context switches until
	// the main thread is ready to run, we set the decrementer to a very
	// large value here to avoid spurious IRQs during startup.
	// Note bit 63 is NOT set, otherwise the decrementer IRQ would fire
	// immediately.
	mtdec(0xfffffffffffffffULL);
}

void z_powerpc_arch_switch(void *newt, void **oldt);

static inline void arch_switch(void *switch_to, void **switched_from)
{
	z_powerpc_arch_switch(switch_to, switched_from);
}

FUNC_NORETURN void z_powerpc_fatal_error(unsigned int reason,
				       const struct arch_esf *esf);

static inline bool arch_is_in_isr(void)
{
	return _current_cpu->nested != 0U;
}

#ifdef CONFIG_IRQ_OFFLOAD
void z_irq_do_offload(void);
#endif

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* ZEPHYR_ARCH_POWERPC_INCLUDE_KERNEL_ARCH_FUNC_H_ */

/*
 * Copyright (c) 2018 - 2020 Antmicro <www.antmicro.com>
 * Copyright (c) 2021 Jonathan Currier
 * Copyright (c) 2021 - 2024 Raptor Engineering, LLC <support@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <mem.h>

/ {
	#address-cells = <1>;
	#size-cells = <1>;
	compatible = "litex,kestrel", "litex-dev";
	model = "litex,kestrel";

	chosen {
		zephyr,entropy = &rng0;
	};

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		ibm,powerpc-cpu-features {
			display-name = "kestrel";
			isa = <3000>;
			device_type = "cpu-features";
			compatible = "ibm,powerpc-cpu-features";

			mmu-radix {
				isa = <3000>;
				usable-privilege = <2>;
				os-support = <0x00>;
			};

			little-endian {
				isa = <0>;
				usable-privilege = <3>;
				os-support = <0x00>;
			};

			cache-inhibited-large-page {
				isa = <0x00>;
				usable-privilege = <2>;
				os-support = <0x00>;
			};

			fixed-point-v3 {
				isa = <3000>;
				usable-privilege = <3>;
			};

			no-execute {
				isa = <0x00>;
				usable-privilege = <2>;
				os-support = <0x00>;
			};

			rng0: random-number-generator {
				compatible = "ibm,builtin-darn";
				status = "okay";
				isa = <3000>;
				usable-privilege = <2>;
				os-support = <0x00>;
			};
		};

		PowerPC,kestrel@0 {
			i-cache-sets = <2>;
			ibm,dec-bits = <64>;
			reservation-granule-size = <64>;
			clock-frequency = <59615384>;
			i-tlb-sets = <1>;
			ibm,ppc-interrupt-server#s = <0>;
			i-cache-block-size = <64>;
			d-cache-block-size = <64>;
			ibm,pa-features = [40 00 
				c2 27 00 00 00 80 00 00 00 00 00 00 00 00 00 00
				00 00 00 00 00 00 00 00 80 00 80 00 80 00 00 00
				80 00 80 00 00 00 80 00 80 00 80 00 80 00 80 00
				80 00 80 00 80 00 80 00 80 00 80 00 80 00 80 00];
			d-cache-sets = <2>;
			ibm,pir = <0x3c>;
			i-tlb-size = <64>;
			cpu-version = <0x990000>;
			status = "okay";
			i-cache-size = <0x1000>;
			ibm,processor-radix-AP-encodings = <0x0c 0xa0000010 0x20000015 0x4000001e>;
			tlb-size = <0>;
			tlb-sets = <0>;
			device_type = "cpu";
			d-tlb-size = <128>;
			d-tlb-sets = <2>;
			reg = <0>;
			general-purpose;
			64-bit;
			d-cache-size = <0x1000>;
			ibm,chip-id = <0x00>;
		};
	};

	soc {
		compatible = "litex,kestrel";
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;

		sram@1000000 {
			compatible = "mmio-sram";
			reg = <0x01000000 0x2000>;
			//ranges = <0x0 0x01000000 0x2000>;
			ranges;
			#address-cells = <1>;
			#size-cells = <1>;
		
			soc-sram@41000000 {
				reg = <0x41000000 0x2000>;
				export;
				//status = "disabled";
			};
		};

		soc_ctrl0: soc_controller@c0000000 {
			compatible = "litex,soc-controller";
			reg = <0xc0000000 0xc>;
			status = "disabled";
		};

		icp0: interrupt-controller@c3ff0000 {
			compatible = "openpower,xics-presentation", "ibm,ppc-xicp";
			ibm,interrupt-server-ranges = <0x0 0x1>;
			reg = <0xc3ff0000 0x1000>;
			status = "okay";
		};

		ics0: interrupt-controller@c3ff1000 {
			compatible = "openpower,xics-sources";
			interrupt-controller;
			reg = <0xc3ff1000 0x1000>;
			#address-cells = <0>;
			#size-cells = <0>;
			#interrupt-cells = <2>;
			status = "okay";
		};

		uart0: serial@c0002000 {
			compatible = "litex,uart";
			interrupt-parent = <&ics0>;
			interrupts = <0 10>;
			reg = <0xc0002000 0x1
				0xc0002004 0x1
				0xc0002008 0x1
				0xc000200c 0x1
				0xc0002010 0x1
				0xc0002014 0x1
				0xc0002018 0x1
				0xc000201c 0x1>;
			reg-names =
				"rxtx",
				"txfull",
				"rxempty",
				"ev_status",
				"ev_pending",
				"ev_enable",
				"txempty",
				"rxfull";
			status = "disabled";
		};


		uart1: serial@c000d000 {
			compatible = "litex,uart";
			interrupt-parent = <&ics0>;
			interrupts = <11 10>;
			reg = <0xc000d000 0x4
				0xc000d004 0x1
				0xc000d008 0x1
				0xc000d00c 0x1
				0xc000d010 0x1
				0xc000d014 0x1
				0xc000d018 0x1
				0xc000d01c 0x1
				0xc000d800 0x4
				0xc000d810 0x4
				0xc000d820 0x4>;
			reg-names =
				"rxtx",
				"txfull",
				"rxempty",
				"ev_status",
				"ev_pending",
				"ev_enable",
				"txempty",
				"rxfull",
				"bus_freq",
				"phy_tuning",
				"phy_config";
			status = "disabled";
		};

		uart2: serial@c000e000 {
			compatible = "litex,uart";
			interrupt-parent = <&ics0>;
			interrupts = <12 10>;
			reg = <0xc000e000 0x4
				0xc000e004 0x1
				0xc000e008 0x1
				0xc000e00c 0x1
				0xc000e010 0x1
				0xc000e014 0x1
				0xc000e018 0x1
				0xc000e01c 0x1
				0xc000e800 0x4
				0xc000e810 0x4
				0xc000e820 0x4>;
			reg-names =
				"rxtx",
				"txfull",
				"rxempty",
				"ev_status",
				"ev_pending",
				"ev_enable",
				"txempty",
				"rxfull",
				"bus_freq",
				"phy_tuning",
				"phy_config";
			status = "disabled";
		};

		uart3: serial@c000f000 {
			compatible = "litex,uart";
			interrupt-parent = <&ics0>;
			interrupts = <13 10>;
			reg = <0xc000f000 0x4
				0xc000f004 0x1
				0xc000f008 0x1
				0xc000f00c 0x1
				0xc000f010 0x1
				0xc000f014 0x1
				0xc000f018 0x1
				0xc000f01c 0x1
				0xc000f800 0x4
				0xc000f810 0x4
				0xc000f820 0x4>;
			reg-names =
				"rxtx",
				"txfull",
				"rxempty",
				"ev_status",
				"ev_pending",
				"ev_enable",
				"txempty",
				"rxfull",
				"bus_freq",
				"phy_tuning",
				"phy_config";
			status = "disabled";
		};

		gpio_discrete: gpio@c0005800 {
			compatible = "litex,gpio";
			reg = <0xc0005800 0xc
				0xc000580c 0xc
				0xc0005818 0xc
				0xc0005824 0x1
				0xc0005830 0x1
				0xc0005848 0x1
				0xc0005854 0x1>;
			reg-names = "oe",
				"in",
				"out",
				"irq_mode",
				"irq_edge",
				"irq_pend",
				"irq_en";
			interrupt-parent = <&ics0>;
			interrupts = <1 9>;
			ngpios = <19>;
			status = "disabled";
			gpio-controller;
			tristatable;
			#gpio-cells = <2>;
		};

		gpio_alphanumeric: gpio@c0006000 {
			compatible = "litex,gpio";
			reg = <0xc0006000 0x10>;
			ngpios = <32>;
			status = "disabled";
			gpio-controller;
			#gpio-cells = <2>;
		};

		rtc@c3005000 {
			compatible = "rcs,simple-rtc";
			reg = <0xc3005000 0x80>;
		};

		fsi@c3007000 {
			compatible = "rcs,swiftfsi";
			reg = <0xc3007000 0x80>;
		};

		spi0: spi@c3004000 {
			compatible = "rcs,tercel";
			status = "disabled";
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			reg = <0xc3004000 0x80
				0xc4000000 0x01000000>;
			reg-names = "control", "data";
		};

		spi1: spi@c3004100 {
			compatible = "rcs,tercel";
			status = "disabled";
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			reg = <0xc3004100 0x80
				0xc6000000 0x01000000>;
			reg-names = "control", "data";
		};

		spi2: spi@c3004200 {
			compatible = "rcs,tercel";
			status = "disabled";
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			reg = <0xc3004200 0x80
				0xc8000000 0x04000000>;
			reg-names = "control", "data";
		};

		i2c0: i2c@c3008000 {
			compatible = "rcs,i2c-ocores";
			status = "disabled";
			opencores,ip-clock-frequency = <0x03938700>;
			opencores,reg_offset = <0x10>;
			clock-frequency = <100000>;
			reg-io-width = <1>;
			reg = <0xc3008000 0x20>;
			interrupt-parent = <&ics0>;
			interrupts = <5 0x1>;
		
			#address-cells = <0x01>;
			#size-cells = <0x00>;
			/* Dummy I2C device for testing. */
			fram0@50 {
				compatible = "atmel,24c256";
				status = "disabled";
				address-width = <16>;
				reg = <0x50>;
			};
		};

		i2c1: i2c@c3008020 {
			compatible = "rcs,i2c-ocores";
			status = "disabled";
			opencores,ip-clock-frequency = <0x03938700>;
			opencores,reg_offset = <0x10>;
			clock-frequency = <100000>;
			reg-io-width = <1>;
			reg = <0xc3008020 0x20>;
			#address-cells = <0x01>;
			#size-cells = <0x00>;

			interrupt-parent = <&ics0>;
			interrupts = <6 0x1>;
		};

		i2c2: i2c@c3008040 {
			compatible = "rcs,i2c-ocores";
			status = "disabled";
			opencores,ip-clock-frequency = <0x03938700>;
			opencores,reg_offset = <0x10>;
			clock-frequency = <100000>;
			reg-io-width = <1>;
			reg = <0xc3008040 0x20>;
			interrupt-parent = <&ics0>;
			interrupts = <7 0x1>;

			#address-cells = <0x01>;
			#size-cells = <0x00>;
		};

		i2c3: i2c@c3008060 {
			compatible = "rcs,i2c-ocores";
			status = "disabled";
			opencores,ip-clock-frequency = <0x03938700>;
			opencores,reg_offset = <0x10>;
			clock-frequency = <100000>;
			reg-io-width = <1>;
			reg = <0xc3008060 0x20>;
			interrupt-parent = <&ics0>;
			interrupts = <8 0x1>;

			#address-cells = <0x01>;
			#size-cells = <0x00>;
		};

		i2c4: i2c@c3008080 {
			compatible = "rcs,i2c-ocores";
			status = "disabled";
			opencores,ip-clock-frequency = <0x03938700>;
			opencores,reg_offset = <0x10>;
			clock-frequency = <100000>;
			reg-io-width = <1>;
			reg = <0xc3008080 0x20>;

			#address-cells = <0x01>;
			#size-cells = <0x00>;
			rtc1: rx8900@32 {
				compatible = "epson,rx8900";
				status = "disabled";
				reg = <0x32>;
				backup-switch-mode = "backup";
			};
		};

		onewire0: w1@c3008800 {
			compatible = "zephyr,w1-master-opencores";
			opencores,ip-clock-frequency = <0x03938700>;
			reg = <0xc3008800 0x20>;
			interrupt-parent = <&ics0>;
			interrupts = <10 0x1>;
			status = "disabled";
		};

		wdt0: watchdog@c0002800 {
			compatible = "litex,watchdog";
			reg = <0xc0002800 0x4>,
				<0xc0002810 0x4>,
				<0xc0002820 0x4>,
				<0xc0002830 0x4>,
				<0xc0002834 0x4>,
				<0xc0002838 0x4>;
			reg-names = "control",
				"cycles",
				"remaining",
				"ev_status",
				"ev_pending",
				"ev_enable";
			interrupt-parent = <&ics0>;
			interrupts = <9 0xd>;
		};

		mdio0: mdio@c0004800 {
			compatible = "litex,liteeth-mdio";
			reg = <0xc0004800 0x4>,
				<0xc0004808 0x4>,
				<0xc000480c 0x4>;
			reg-names = "crg_reset",
				"mdio_w",
				"mdio_r";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";

			phy0: ethernet-phy@0 {
				compatible = "ethernet-phy";
				no-reset;
				reg = <0>;
			};
		};

		eth0: ethernet@c0005000 {
			compatible = "litex,liteeth";
			interrupt-parent = <&ics0>;
			interrupts = <2 0>;
			reg = <0xc0005000 0x4
				0xc0005004 0x16
				0xc0005028 0x4
				0xc000502c 0x4
				0xc0005030 0x4
				0xc0005034 0x4
				0xc000503c 0x4
				0xc0005040 0x8
				0xc000504c 0x4
				0xc3002000 0x2000>;
			local-mac-address = [2c 09 4d 00 00 00];
			reg-names =
				"rx_slot",
				"rx_length",
				"rx_ev_pending",
				"rx_ev_enable",
				"tx_start",
				"tx_ready",
				"tx_slot",
				"tx_length",
				"tx_ev_pending",
				"buffers";
			phy-handle = <&phy0>;
			status = "disabled";
		};
	};
};

/*
 * Copyright (c) 2021-2022 Raptor Engineering, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT rcs_tercel

#define LOG_LEVEL CONFIG_SPI_LOG_LEVEL
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(spi_tercel);

#include <zephyr/sys/sys_io.h>
#include <zephyr/drivers/spi.h>

#include "spi_context.h"
#include "spi_tercel.h"

static int spi_tercel_configure(const struct spi_tercel_cfg *info,
				struct spi_tercel_data *spi,
				const struct spi_config *config)
{
	uint32_t dword;

	if (spi_context_configured(&spi->ctx, config)) {
		// No (re)configuration required
		return 0;
	}

	// Tercel SPI only supports master mode
	if (spi_context_is_slave(&spi->ctx)) {
		LOG_ERR("Slave mode not supported");
		return -ENOTSUP;

	}
	if (config->operation & (SPI_MODE_LOOP | SPI_TRANSFER_LSB |
				 SPI_LINES_DUAL)) {
		LOG_ERR("Unsupported configuration");
		return -EINVAL;
	}

	// SPI physical mode
	if ((!(SPI_MODE_GET(config->operation) & SPI_MODE_CPOL)) && (SPI_MODE_GET(config->operation) & SPI_MODE_CPHA)) {
		// Mode 1 operation requested
		// The Tercel PHY only supports Mode 0 and Mode 3 operation
		LOG_ERR("Unsupported PHY configuration");
		return -EINVAL;
	}
	if ((SPI_MODE_GET(config->operation) & SPI_MODE_CPOL) && (SPI_MODE_GET(config->operation) & SPI_MODE_CPHA)) {
		// Mode 2 operation requested
		// The Tercel PHY only supports Mode 0 and Mode 3 operation
		LOG_ERR("Unsupported PHY configuration");
		return -EINVAL;
	}

	// SPI physical bus width
	// If not specified in operation field, leave at BIOS-configured default
	if (config->operation & SPI_LINES_QUAD) {
		// Set SPI controller to QSPI mode
		dword = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1);
		dword &= ~(TERCEL_SPI_PHY_IO_TYPE_MASK << TERCEL_SPI_PHY_IO_TYPE_SHIFT);
		dword |= ((TERCEL_SPI_PHY_IO_TYPE_QUAD & TERCEL_SPI_PHY_IO_TYPE_MASK) << TERCEL_SPI_PHY_IO_TYPE_SHIFT);
		write_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);
		LOG_INF("Quad SPI mode enabled");
	}
	else if (config->operation & SPI_LINES_SINGLE) {
		// Set SPI controller to single SPI mode
		dword = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1);
		dword &= ~(TERCEL_SPI_PHY_IO_TYPE_MASK << TERCEL_SPI_PHY_IO_TYPE_SHIFT);
		dword |= ((TERCEL_SPI_PHY_IO_TYPE_SINGLE & TERCEL_SPI_PHY_IO_TYPE_MASK) << TERCEL_SPI_PHY_IO_TYPE_SHIFT);
		write_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);
		LOG_INF("Single SPI mode enabled");
	}

	// Configure SPI clock cycle divider for specified frequency
	uint32_t spi_sys_clock_freq = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CLK_FREQ);
	uint32_t best_spi_divisor = (spi_sys_clock_freq / (config->frequency * 2)) - 1;
	dword = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1);
	dword &= ~(TERCEL_SPI_PHY_CLOCK_DIVISOR_MASK << TERCEL_SPI_PHY_CLOCK_DIVISOR_SHIFT);
	dword |= ((best_spi_divisor & TERCEL_SPI_PHY_CLOCK_DIVISOR_MASK) << TERCEL_SPI_PHY_CLOCK_DIVISOR_SHIFT);
	write_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

	// Calculate and dump configured SPI clock speed
	uint8_t spi_divisor = (read_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1) >> TERCEL_SPI_PHY_CLOCK_DIVISOR_SHIFT) & TERCEL_SPI_PHY_CLOCK_DIVISOR_MASK;
	spi_divisor = (spi_divisor + 1) * 2;
	uint8_t spi_dummy_cycles = (read_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1) >> TERCEL_SPI_PHY_DUMMY_CYCLES_SHIFT) & TERCEL_SPI_PHY_DUMMY_CYCLES_MASK;
	LOG_INF("Tercel SPI controller frequency configured to %d MHz (bus frequency %d MHz, dummy cycles %d)",
		(read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CLK_FREQ) / spi_divisor) / 1000000,
		read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CLK_FREQ) / 1000000, spi_dummy_cycles);


	spi->ctx.config = config;

	spi_context_cs_configure_all(&spi->ctx);

	return 0;
}

int spi_tercel_transceive(const struct device *dev,
			     const struct spi_config *config,
			     const struct spi_buf_set *tx_bufs,
			     const struct spi_buf_set *rx_bufs)
{
	const struct spi_tercel_cfg *info = dev->config;
	struct spi_tercel_data *spi = SPI_TERCEL_DATA(dev);
	struct spi_context *ctx = &spi->ctx;

	size_t i;
	size_t cur_xfer_len;
	int rc;

	/* Lock the SPI Context */
	spi_context_lock(ctx, false, NULL, NULL, config);

	spi_tercel_configure(info, spi, config);

	// Set user mode
	write_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_CTL1,
				read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_CTL1) |
				(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

	spi_context_buffers_setup(ctx, tx_bufs, rx_bufs, 1);

	while (spi_context_tx_buf_on(ctx) || spi_context_rx_buf_on(ctx)) {
		cur_xfer_len = spi_context_longest_current_buf(ctx);

		for (i = 0; i < cur_xfer_len; i++) {
			// Write byte
			if (spi_context_tx_buf_on(ctx)) {
				sys_write8(*ctx->tx_buf,
					   info->data);
				spi_context_update_tx(ctx, 1, 1);
			}
			// Receive and store byte if rx buffer is on
			if (spi_context_rx_on(ctx)) {
				/* Get received byte */
				*ctx->rx_buf = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_DATA1);
				spi_context_update_rx(ctx, 1, 1);
			}
		}
	}

	// Clear user mode
	write_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_CTL1,
				read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_CTL1) &
				~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

	spi_context_complete(ctx, dev, 0);
	rc = spi_context_wait_for_completion(ctx);

	spi_context_release(ctx, rc);

	return rc;
}

#ifdef CONFIG_SPI_ASYNC
static int spi_tercel_transceive_async(const struct device *dev,
					  const struct spi_config *config,
					  const struct spi_buf_set *tx_bufs,
					  const struct spi_buf_set *rx_bufs,
					  struct k_poll_signal *async)
{
	return -ENOTSUP;
}
#endif /* CONFIG_SPI_ASYNC */

int spi_tercel_release(const struct device *dev,
			  const struct spi_config *config)
{
	spi_context_unlock_unconditionally(&SPI_TERCEL_DATA(dev)->ctx);
	return 0;
}

static struct spi_driver_api spi_tercel_api = {
	.transceive = spi_tercel_transceive,
	.release = spi_tercel_release,
#ifdef CONFIG_SPI_ASYNC
	.transceive_async = spi_tercel_transceive_async,
#endif /* CONFIG_SPI_ASYNC */
};

int spi_tercel_init(const struct device *dev)
{
	const struct spi_tercel_cfg *info = dev->config;
	uint32_t dword;

	if ((read_tercel_register(info->base, TERCEL_SPI_REG_DEVICE_ID_HIGH) != TERCEL_SPI_DEVICE_ID_HIGH) ||
		(read_tercel_register(info->base, TERCEL_SPI_REG_DEVICE_ID_LOW) != TERCEL_SPI_DEVICE_ID_LOW))
	{
		return -ENODEV;
	}

	uint32_t tercel_version = read_tercel_register(info->base, TERCEL_SPI_REG_DEVICE_VERSION);
	LOG_INF("Raptor Tercel SPI master found, device version %0d.%0d.%d 0x%08x/0x%08x",
		(tercel_version >> TERCEL_SPI_VERSION_MAJOR_SHIFT) & TERCEL_SPI_VERSION_MAJOR_MASK,
		(tercel_version >> TERCEL_SPI_VERSION_MINOR_SHIFT) & TERCEL_SPI_VERSION_MINOR_MASK,
		(tercel_version >> TERCEL_SPI_VERSION_PATCH_SHIFT) & TERCEL_SPI_VERSION_PATCH_MASK,
		info->base, info->data);

	// Clear user mode, if active
	write_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_CTL1,
				read_tercel_register(info->base, TERCEL_SPI_REG_SYS_CORE_CTL1) &
				~(TERCEL_SPI_ENABLE_USER_MODE_MASK << TERCEL_SPI_ENABLE_USER_MODE_SHIFT));

	// Set extra CS delay cycle count to 0
	dword = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1);
	dword &= ~(TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_MASK << TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_SHIFT);
	dword |= ((0 & TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_MASK) << TERCEL_SPI_PHY_CS_EXTRA_IDLE_CYC_SHIFT);
	write_tercel_register(info->base, TERCEL_SPI_REG_SYS_PHY_CFG1, dword);

	// Set maximum CS assert cycle count to 10000
	dword = read_tercel_register(info->base, TERCEL_SPI_REG_SYS_FLASH_CFG4);
	dword &= ~(TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_MASK << TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_SHIFT);
	dword |= ((10000 & TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_MASK) << TERCEL_SPI_FLASH_CS_EN_LIMIT_CYC_SHIFT);
	write_tercel_register(info->base, TERCEL_SPI_REG_SYS_FLASH_CFG4, dword);

	/* Make sure the context is unlocked */
	spi_context_unlock_unconditionally(&SPI_TERCEL_DATA(dev)->ctx);

	return 0;
}

#define SPI_TERCEL_INIT(inst)						\
	static struct spi_tercel_cfg spi_tercel_cfg_##inst = {	\
		.base = DT_INST_REG_ADDR_BY_NAME(inst, control),	\
		.data = DT_INST_REG_ADDR_BY_NAME(inst, data),	\
	};								\
									\
	static struct spi_tercel_data spi_tercel_data_##inst = {	\
		SPI_CONTEXT_INIT_LOCK(spi_tercel_data_##inst, ctx),	\
		SPI_CONTEXT_INIT_SYNC(spi_tercel_data_##inst, ctx),	\
	};								\
									\
	DEVICE_DT_INST_DEFINE(inst,					\
			    spi_tercel_init,				\
			    NULL,					\
			    &spi_tercel_data_##inst,			\
			    &spi_tercel_cfg_##inst,			\
			    POST_KERNEL,				\
			    CONFIG_SPI_INIT_PRIORITY,			\
			    &spi_tercel_api);

DT_INST_FOREACH_STATUS_OKAY(SPI_TERCEL_INIT)

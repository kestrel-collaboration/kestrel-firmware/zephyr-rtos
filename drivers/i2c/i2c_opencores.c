/*
 * Copyright (c) 2020 - 2024 Raptor Engineering, LLC
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
 */

#define DT_DRV_COMPAT rcs_i2c_ocores

#include <zephyr/kernel.h>
#include <zephyr/drivers/i2c.h>

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(i2c_opencores, CONFIG_I2C_LOG_LEVEL);

#include <errno.h>

#include "i2c-priv.h"
#include "i2c_opencores.h"

struct i2c_opencores_config {
	uintptr_t base_address;
	uint64_t system_frequency;
	uint64_t i2c_bus_frequency;
};

struct i2c_opencores_data {
	bool transceiver_busy;
};

int i2c_opencores_configure(const struct device *dev, uint32_t dev_config)
{
	const struct i2c_opencores_config *config;
	struct i2c_opencores_data *data;

	if (!dev) {
		LOG_ERR("Invalid device handle");
		return -EINVAL;
	}

	config = dev->config;
	if (!config) {
		LOG_ERR("Invalid device configuration");
		return -EINVAL;
	}

	data = dev->data;
	if (!config) {
		LOG_ERR("Invalid volatile data structure");
		return -EINVAL;
	}

	if (!config->base_address) {
		LOG_ERR("Invalid base address");
		return -EINVAL;
	}

	data->transceiver_busy = true;

	// Input data seems OK, configure device if present...
	LOG_INF("Configuring I2C master at address %p...", (void*)config->base_address);

	if ((*((volatile uint32_t *)(config->base_address + OPENCORES_I2C_MASTER_DEVICE_ID_HIGH)) != OPENCORES_I2C_DEVICE_ID_HIGH) ||
		(*((volatile uint32_t *)(config->base_address + OPENCORES_I2C_MASTER_DEVICE_ID_LOW)) != OPENCORES_I2C_DEVICE_ID_LOW))
	{
		data->transceiver_busy = false;
		return -ENODEV;
	}
	uint32_t opencores_spi_version = *((volatile uint32_t *)(config->base_address + OPENCORES_I2C_MASTER_DEVICE_VERSION));
	LOG_INF("OpenCores I2C master found, device version %0d.%0d.%d",
		(opencores_spi_version >> OPENCORES_I2C_VERSION_MAJOR_SHIFT) & OPENCORES_I2C_VERSION_MAJOR_MASK,
		(opencores_spi_version >> OPENCORES_I2C_VERSION_MINOR_SHIFT) & OPENCORES_I2C_VERSION_MINOR_MASK,
		(opencores_spi_version >> OPENCORES_I2C_VERSION_PATCH_SHIFT) & OPENCORES_I2C_VERSION_PATCH_MASK);

	// Compute prescale value from system clock and desired I2C frequency in HZ
	uint16_t i2c_prescale = (config->system_frequency / (5LL * config->i2c_bus_frequency)) - 1;
	LOG_DBG("Desired prescale register: 0x%04x (system clock %lldMHz, bus frequency "
		"%lldkHz)",
		i2c_prescale, config->system_frequency / 1000000LL, config->i2c_bus_frequency / 1000LL);
	*((volatile uint8_t *)(config->base_address + OPENCORES_I2C_MASTER_PRESCALE_LOW)) = i2c_prescale & 0xff;
	*((volatile uint8_t *)(config->base_address + OPENCORES_I2C_MASTER_PRESCALE_HIGH)) = (i2c_prescale >> 8) & 0xff;
	if ((*((volatile uint8_t *)(config->base_address + OPENCORES_I2C_MASTER_PRESCALE_LOW)) == (i2c_prescale & 0xff)) &&
		(*((volatile uint8_t *)(config->base_address + OPENCORES_I2C_MASTER_PRESCALE_HIGH)) == ((i2c_prescale >> 8) & 0xff)))
	{
		LOG_INF("Enabling I2C core");
		*((volatile uint8_t *)(config->base_address + OPENCORES_I2C_MASTER_PRESCALE_CTL)) =
		(OPENCORES_I2C_MASTER_CTL_CORE_EN_MASK << OPENCORES_I2C_MASTER_CTL_CORE_EN_SHIFT);

		data->transceiver_busy = false;
		return 0;
	}

	data->transceiver_busy = false;
	return -EIO;
}

static int write_i2c_data(uint8_t *base_address, uint8_t slave_address, uint8_t *data, int data_length, uint8_t send_stop_signal)
{
	uint32_t i2c_op_timeout_counter;
	uint8_t i2c_op_failed;
	int active_byte;
	uint8_t byte;

	i2c_op_failed = 0;
	*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX)) =
		(slave_address << 1) | (OPENCORES_I2C_MASTER_TX_RX_WRITE_MASK << OPENCORES_I2C_MASTER_TX_RX_WRITE_SHIFT);
	*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) =
		(OPENCORES_I2C_MASTER_CMD_STA_MASK << OPENCORES_I2C_MASTER_CMD_STA_SHIFT) | (OPENCORES_I2C_MASTER_CMD_WR_MASK << OPENCORES_I2C_MASTER_CMD_WR_SHIFT);
	i2c_op_timeout_counter = 0;
	while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
		OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
	{
		if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
		{
			LOG_WRN("I2C operation timed out in device select!");
			i2c_op_failed = 1;
			break;
		}
		k_usleep(100);
		i2c_op_timeout_counter++;
	}

	if (!i2c_op_failed)
	{
		if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT) &
		OPENCORES_I2C_MASTER_STATUS_RXACK_MASK)
		{
			LOG_WRN("I2C operation failed in device select!");
			i2c_op_failed = 1;
		}
	}

	for (active_byte = 0; active_byte < data_length; active_byte++)
	{
		if (!i2c_op_failed)
		{
			*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX)) = *(data + active_byte);
			byte = OPENCORES_I2C_MASTER_CMD_WR_MASK << OPENCORES_I2C_MASTER_CMD_WR_SHIFT;
			if ((active_byte + 1) == data_length)
			{
				// Final byte
				if (send_stop_signal)
				{
					byte |= OPENCORES_I2C_MASTER_CMD_STO_MASK << OPENCORES_I2C_MASTER_CMD_STO_SHIFT;
				}
			}
			*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) = byte;
			i2c_op_timeout_counter = 0;
			while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
				OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
			{
				if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
				{
					LOG_WRN("I2C operation timed out in register write!");
					i2c_op_failed = 1;
					break;
				}
				k_usleep(100);
				i2c_op_timeout_counter++;
			}
		}

		if (!i2c_op_failed)
		{
			if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT) &
				OPENCORES_I2C_MASTER_STATUS_RXACK_MASK)
			{
				LOG_WRN("I2C operation failed in register write!");
				i2c_op_failed = 1;
			}
		}

		if (i2c_op_failed)
		{
			break;
		}
	}

	return i2c_op_failed;
}

static int read_i2c_data(uint8_t *base_address, uint8_t slave_address, uint8_t *data, int *data_length, int max_data_length, uint8_t send_stop_signal)
{
	uint32_t i2c_op_timeout_counter;
	uint8_t i2c_op_failed;
	int active_byte;
	uint8_t byte;

	i2c_op_failed = 0;
	*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX)) =
		(slave_address << 1) | (OPENCORES_I2C_MASTER_TX_RX_READ_MASK << OPENCORES_I2C_MASTER_TX_RX_READ_SHIFT);
	*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) =
		(OPENCORES_I2C_MASTER_CMD_STA_MASK << OPENCORES_I2C_MASTER_CMD_STA_SHIFT) | (OPENCORES_I2C_MASTER_CMD_WR_MASK << OPENCORES_I2C_MASTER_CMD_WR_SHIFT);
	i2c_op_timeout_counter = 0;
	while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
		OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
	{
		if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
		{
			LOG_WRN("I2C operation timed out in device select!");
			i2c_op_failed = 1;
			break;
		}
		k_usleep(100);
		i2c_op_timeout_counter++;
	}

	if (!i2c_op_failed)
	{
		if ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_RXACK_SHIFT) &
		OPENCORES_I2C_MASTER_STATUS_RXACK_MASK)
		{
			LOG_WRN("I2C operation failed in device select!");
			i2c_op_failed = 1;
		}
	}

	if (data_length)
	{
		*data_length = 0;
	}
	for (active_byte = 0; active_byte < max_data_length; active_byte++)
	{
		if (!i2c_op_failed)
		{
			if ((active_byte + 1) == max_data_length)
			{
				// Final byte, send NACK
				byte = (OPENCORES_I2C_MASTER_CMD_RD_MASK << OPENCORES_I2C_MASTER_CMD_RD_SHIFT) |
				(OPENCORES_I2C_MASTER_CMD_NACK_MASK << OPENCORES_I2C_MASTER_CMD_NACK_SHIFT);
				if (send_stop_signal)
				{
					byte |= OPENCORES_I2C_MASTER_CMD_STO_MASK << OPENCORES_I2C_MASTER_CMD_STO_SHIFT;
				}
				*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) = byte;
			}
			else
			{
				// More bytes expected, send ACK
				*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) =
				(OPENCORES_I2C_MASTER_CMD_RD_MASK << OPENCORES_I2C_MASTER_CMD_RD_SHIFT) |
				(OPENCORES_I2C_MASTER_CMD_ACK_MASK << OPENCORES_I2C_MASTER_CMD_ACK_SHIFT);
			}
			while ((*((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_CMD_STATUS)) >> OPENCORES_I2C_MASTER_STATUS_TIP_SHIFT) &
				OPENCORES_I2C_MASTER_STATUS_TIP_MASK)
			{
				if (i2c_op_timeout_counter > I2C_MASTER_OPERATION_TIMEOUT_VALUE)
				{
					LOG_WRN("I2C operation timed out in register read!");
					i2c_op_failed = 1;
					break;
				}
				k_usleep(100);
				i2c_op_timeout_counter++;
			}
			if (!i2c_op_failed)
			{
				*(data + active_byte) = *((volatile uint8_t *)(base_address + OPENCORES_I2C_MASTER_TX_RX));
				if (data_length)
				{
					*data_length = *data_length + 1;
				}
			}
		}

		if (i2c_op_failed)
		{
			break;
		}
	}

	return i2c_op_failed;
}

int i2c_opencores_transfer(const struct device *dev,
			   struct i2c_msg *msgs, uint8_t num_msgs,
			   uint16_t slave_address)
{
	const struct i2c_opencores_config *config;
	struct i2c_opencores_data *data;
	int bytes_read;
	int ret;

	if (!dev) {
		LOG_ERR("Invalid device handle");
		return -EINVAL;
	}

	config = dev->config;
	if (!config) {
		LOG_ERR("Invalid device configuration");
		return -EINVAL;
	}

	data = dev->data;
	if (!config) {
		LOG_ERR("Invalid volatile data structure");
		return -EINVAL;
	}

	if (!config->base_address) {
		LOG_ERR("Invalid base address");
		return -EINVAL;
	}

	if (!msgs) {
		LOG_ERR("Invalid message buffer");
		return -EINVAL;
	}

	// Wait for any currently active transfers to complete
	while (data->transceiver_busy) {
		k_usleep(100);
	}

	for (int i = 0; i < num_msgs; i++) {
		if (msgs[i].flags & I2C_MSG_READ) {
			data->transceiver_busy = true;
			ret = read_i2c_data((uint8_t *)config->base_address, slave_address, msgs[i].buf, &bytes_read, msgs[i].len, 1);
			data->transceiver_busy = false;

			if (bytes_read != msgs[i].len) {
				ret = -EIO;
			}
		}
		else {
			data->transceiver_busy = true;
			ret = write_i2c_data((uint8_t *)config->base_address, slave_address, msgs[i].buf, msgs[i].len, 1);
			data->transceiver_busy = false;
		}

		if (ret) {
			return ret;
		}
	}

	return 0;
}

static int i2c_opencores_init(const struct device *dev)
{
	const struct i2c_opencores_config *config = dev->config;
	uint32_t dev_config;
	int ret;

	dev_config = (I2C_MODE_CONTROLLER | i2c_map_dt_bitrate(config->i2c_bus_frequency));

	ret = i2c_opencores_configure(dev, dev_config);
	if (ret) {
		LOG_ERR("I2C controller initial configuration failed");
		return ret;
	}

	return 0;
}

static const struct i2c_driver_api i2c_opencores_api = {
	.configure = i2c_opencores_configure,
	.transfer = i2c_opencores_transfer,
};

#define I2C_OPENCORES_INIT(n) \
	static struct i2c_opencores_config i2c_opencores_config_##n = {            \
		.base_address = DT_INST_REG_ADDR(n),                               \
		.system_frequency = DT_INST_PROP(n, opencores_ip_clock_frequency), \
		.i2c_bus_frequency = DT_INST_PROP(n, clock_frequency),             \
	};                                                                         \
	static struct i2c_opencores_data i2c_opencores_data_##n;                   \
	I2C_DEVICE_DT_INST_DEFINE(n,                                               \
			    i2c_opencores_init,                                    \
			    NULL,                                                  \
			    &i2c_opencores_data_##n,                                                  \
			    &i2c_opencores_config_##n,                             \
			    POST_KERNEL,                                           \
			    CONFIG_I2C_INIT_PRIORITY,                              \
			    &i2c_opencores_api);

DT_INST_FOREACH_STATUS_OKAY(I2C_OPENCORES_INIT)
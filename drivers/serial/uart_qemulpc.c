/*
 * Copyright (c) 2019 Michael Neuling <mikey@neuling.org>
 * Copyright (c) 2019 Anton Blanchard <anton@linux.ibm.com>
 * Copyright (c) 2021 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT qemulpc_uart0

#include <kernel.h>
#include <arch/cpu.h>
#include <init.h>
#include <irq.h>
#include <device.h>
#include <drivers/uart.h>
#include <zephyr/types.h>

#define LPC_UART_BASE   0x60300d00103f8

/* Taken from skiboot */
#define REG_RBR         0
#define REG_THR         0
#define REG_DLL         0
#define REG_IER         1
#define REG_DLM         1
#define REG_FCR         2
#define REG_IIR         2
#define REG_LCR         3
#define REG_MCR         4
#define REG_LSR         5
#define REG_MSR         6
#define REG_SCR         7

#define LSR_DR          0x01  /* Data ready */
#define LSR_OE          0x02  /* Overrun */
#define LSR_PE          0x04  /* Parity error */
#define LSR_FE          0x08  /* Framing error */
#define LSR_BI          0x10  /* Break */
#define LSR_THRE        0x20  /* Xmit holding register empty */
#define LSR_TEMT        0x40  /* Xmitter empty */
#define LSR_ERR         0x80  /* Error */

static uint64_t lpc_uart_base;

static void lpc_uart_reg_write(uint64_t offset, uint8_t val) {
	uint64_t addr;

	addr = lpc_uart_base + offset;

	*(volatile uint8_t *)addr = val;
}

static uint8_t lpc_uart_reg_read(uint64_t offset) {
	uint64_t addr;
	uint8_t val;

	addr = lpc_uart_base + offset;

	val = *(volatile uint8_t *)addr;

	return val;
}

static int lpc_uart_tx_full(void) {
	return !(lpc_uart_reg_read(REG_LSR) & LSR_THRE);
}

static int lpc_uart_rx_empty(void) {
	return !(lpc_uart_reg_read(REG_LSR) & LSR_DR);
}

/**
 * @brief Output a character in polled mode.
 *
 * Writes data to tx register. Waits for space if transmitter is full.
 *
 * @param dev UART device struct
 * @param c Character to send
 */
static void uart_lpcuart_poll_out(const struct device *dev, unsigned char c)
{
	/* wait for space */
	while (lpc_uart_tx_full()) {
	}

	lpc_uart_reg_write(REG_RBR, c);
}

/**
 * @brief Poll the device for input.
 *
 * @param dev UART device struct
 * @param c Pointer to character
 *
 * @return 0 if a character arrived, -1 if the input buffer if empty.
 */
static int uart_lpcuart_poll_in(const struct device *dev, unsigned char *c)
{
	if (!lpc_uart_rx_empty()) {
		*c = lpc_uart_reg_read(REG_THR);
		return 0;
	} else {
		return -1;
	}
}

static const struct uart_driver_api uart_lpcuart_driver_api = {
	.poll_in		= uart_lpcuart_poll_in,
	.poll_out		= uart_lpcuart_poll_out,
	.err_check		= NULL,
};

static int uart_lpcuart_init(const struct device *dev)
{
	lpc_uart_base = LPC_UART_BASE;

	return 0;
}

DEVICE_DT_INST_DEFINE(0,
		uart_lpcuart_init,
		NULL,
		NULL, NULL,
		PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
		(void *)&uart_lpcuart_driver_api);

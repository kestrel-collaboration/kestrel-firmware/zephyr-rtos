/*
 * Copyright (c) 2018 - 2019 Antmicro <www.antmicro.com>
 * Copyright (c) 2021 - 2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT litex_uart

#include <zephyr/kernel.h>
#include <zephyr/arch/cpu.h>
#include <zephyr/init.h>
#include <zephyr/irq.h>
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/types.h>

#include <soc.h>

#define UART_EV_TX		BIT(0)
#define UART_EV_RX		BIT(1)

#define PHY_CONFIG_STOP_BITS_MASK	0x1
#define PHY_CONFIG_STOP_BITS_SHIFT	0
#define PHY_CONFIG_DATA_BITS_MASK	0xf
#define PHY_CONFIG_DATA_BITS_SHIFT	8
#define PHY_CONFIG_PARITY_MASK		0x3
#define PHY_CONFIG_PARITY_SHIFT		16

struct uart_litex_device_config {
	uint32_t rxtx_addr;
	uint32_t txfull_addr;
	uint32_t rxempty_addr;
	uint32_t ev_status_addr;
	uint32_t ev_pending_addr;
	uint32_t ev_enable_addr;
	uint32_t txempty_addr;
	uint32_t rxfull_addr;
	uint32_t phy_tuning_addr;
	uint32_t phy_config_addr;
	uint32_t bus_freq_addr;
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	void (*config_func)(const struct device *dev);
#endif
};

struct uart_litex_data {
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	struct k_timer timer;
	uart_irq_callback_user_data_t callback;
	void *cb_data;
#endif

	struct uart_config uart_config;

#define DEV_DATA(dev)						\
	((struct uart_liteuart_data * const)(dev)->data)
};

/**
 * @brief Output a character in polled mode.
 *
 * Writes data to tx register. Waits for space if transmitter is full.
 *
 * @param dev UART device struct
 * @param c Character to send
 */
static void uart_litex_poll_out(const struct device *dev, unsigned char c)
{
	const struct uart_litex_device_config *config = dev->config;
	/* wait for space */
	while (litex_read8(config->txfull_addr)) {
	}

	litex_write8(c, config->rxtx_addr);
}

/**
 * @brief Poll the device for input.
 *
 * @param dev UART device struct
 * @param c Pointer to character
 *
 * @return 0 if a character arrived, -1 if the input buffer if empty.
 */
static int uart_litex_poll_in(const struct device *dev, unsigned char *c)
{
	const struct uart_litex_device_config *config = dev->config;

	if (!litex_read8(config->rxempty_addr)) {
		*c = litex_read8(config->rxtx_addr);

		/* refresh UART_RXEMPTY by writing UART_EV_RX
		 * to UART_EV_PENDING
		 */
		litex_write8(UART_EV_RX, config->ev_pending_addr);
		return 0;
	} else {
		return -1;
	}
}

#ifdef CONFIG_UART_USE_RUNTIME_CONFIGURE
static int uart_litex_config_get(const struct device *dev,
				struct uart_config *cfg)
{
	struct uart_litex_data *data = dev->data;

	cfg->baudrate = data->uart_config.baudrate;
	cfg->parity = UART_CFG_PARITY_NONE;
	cfg->stop_bits = data->uart_config.stop_bits;
	cfg->flow_ctrl = UART_CFG_FLOW_CTRL_NONE;

	return 0;
}

static int uart_litex_configure(const struct device *dev,
				const struct uart_config *cfg)
{
	const struct uart_litex_device_config *config = dev->config;
	struct uart_litex_data *data = dev->data;

	int valid_request = 1;

	if (cfg->flow_ctrl != UART_CFG_FLOW_CTRL_NONE) {
		valid_request = 0;
	}

	if (!valid_request) {
		return -ENOTSUP;
	}

	if (!config->phy_tuning_addr || !config->phy_config_addr || !config->bus_freq_addr) {
		return -ENOTSUP;
	}

	// Compute new PHY tuning value
	uint32_t bus_frequency = litex_read32(config->bus_freq_addr);
	if (bus_frequency == 0) {
		return -1;
	}
	uint32_t phy_tuning_word = (cfg->baudrate * (1ULL << 32)) / bus_frequency;

	uint32_t phy_config_word = litex_read32(config->phy_config_addr);
	// Compute new stop bits control value
	phy_config_word = phy_config_word & ~(PHY_CONFIG_STOP_BITS_MASK << PHY_CONFIG_STOP_BITS_SHIFT);
	if (cfg->stop_bits == UART_CFG_STOP_BITS_2) {
		phy_config_word = phy_config_word | 0x1 << PHY_CONFIG_STOP_BITS_SHIFT;
	}
	else if (cfg->stop_bits != UART_CFG_STOP_BITS_1) {
		return -ENOTSUP;
	}
	// Compute new data bits control value
	phy_config_word = phy_config_word & ~(PHY_CONFIG_DATA_BITS_MASK << PHY_CONFIG_DATA_BITS_SHIFT);
	switch (cfg->data_bits) {
		case UART_CFG_DATA_BITS_5: phy_config_word = phy_config_word | 0x5 << PHY_CONFIG_DATA_BITS_SHIFT; break;
		case UART_CFG_DATA_BITS_6: phy_config_word = phy_config_word | 0x6 << PHY_CONFIG_DATA_BITS_SHIFT; break;
		case UART_CFG_DATA_BITS_7: phy_config_word = phy_config_word | 0x7 << PHY_CONFIG_DATA_BITS_SHIFT; break;
		case UART_CFG_DATA_BITS_8: phy_config_word = phy_config_word | 0x8 << PHY_CONFIG_DATA_BITS_SHIFT; break;
		default: return -ENOTSUP;
	}
	// Compute new parity control value
	phy_config_word = phy_config_word & ~(PHY_CONFIG_PARITY_MASK << PHY_CONFIG_PARITY_SHIFT);
	switch (cfg->parity) {
		case UART_CFG_PARITY_EVEN: phy_config_word = phy_config_word | 0x2 << PHY_CONFIG_PARITY_SHIFT; break;
		case UART_CFG_PARITY_ODD:  phy_config_word = phy_config_word | 0x3 << PHY_CONFIG_PARITY_SHIFT; break;
		case UART_CFG_PARITY_NONE: phy_config_word = phy_config_word | 0x0 << PHY_CONFIG_PARITY_SHIFT; break;
		default: return -ENOTSUP;
	}

	// Reconfigure PHY
	litex_write32(phy_tuning_word, config->phy_tuning_addr);
	litex_write32(phy_config_word, config->phy_config_addr);

	data->uart_config = *cfg;

	return 0;
}
#endif

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
/**
 * @brief Enable TX interrupt in event register
 *
 * @param dev UART device struct
 */
static void uart_litex_irq_tx_enable(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;
	struct uart_litex_data *data = dev->data;

	uint8_t enable = litex_read8(config->ev_enable_addr);

	litex_write8(enable | UART_EV_TX, config->ev_enable_addr);

	if (!litex_read8(config->txfull_addr)) {
		/*
		 * TX done event already generated an edge interrupt. Generate a
		 * soft interrupt and have it call the callback function in
		 * timer isr context.
		 */
		k_timer_start(&data->timer, K_NO_WAIT, K_NO_WAIT);
	}
}

/**
 * @brief Disable TX interrupt in event register
 *
 * @param dev UART device struct
 */
static void uart_litex_irq_tx_disable(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;

	uint8_t enable = litex_read8(config->ev_enable_addr);

	litex_write8(enable & ~(UART_EV_TX), config->ev_enable_addr);
}

/**
 * @brief Enable RX interrupt in event register
 *
 * @param dev UART device struct
 */
static void uart_litex_irq_rx_enable(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;

	uint8_t enable = litex_read8(config->ev_enable_addr);

	litex_write8(enable | UART_EV_RX, config->ev_enable_addr);
}

/**
 * @brief Disable RX interrupt in event register
 *
 * @param dev UART device struct
 */
static void uart_litex_irq_rx_disable(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;

	uint8_t enable = litex_read8(config->ev_enable_addr);

	litex_write8(enable & ~(UART_EV_RX), config->ev_enable_addr);
}

/**
 * @brief Check if Tx IRQ has been raised and UART is ready to accept new data
 *
 * @param dev UART device struct
 *
 * @return 1 if an IRQ has been raised, 0 otherwise
 */
static int uart_litex_irq_tx_ready(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;

	uint8_t val = litex_read8(config->txfull_addr);

	if (litex_read8(config->ev_enable_addr) & UART_EV_TX) {
		return !val;
	}

	return 0;
}

/**
 * @brief Check if Tx has completed
 *
 * @param dev UART device struct
 *
 * @return 1 if the TX buffer is empty, 0 otherwise
 */
static int uart_litex_irq_tx_complete(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;

	uint8_t val = litex_read8(config->txempty_addr);

	return !!val;
}

/**
 * @brief Check if Rx IRQ has been raised and there's data to be read from UART
 *
 * @param dev UART device struct
 *
 * @return 1 if an IRQ has been raised, 0 otherwise
 */
static int uart_litex_irq_rx_ready(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;

	if (litex_read8(config->ev_enable_addr) & UART_EV_RX) {
		if (!litex_read8(config->rxempty_addr)) {
			return 1;
		}
	}

	return 0;
}

/**
 * @brief Fill FIFO with data
 *
 * @param dev UART device struct
 * @param tx_data Data to transmit
 * @param size Number of bytes to send
 *
 * @return Number of bytes sent
 */
static int uart_litex_fifo_fill(const struct device *dev,
				const uint8_t *tx_data, int size)
{
	const struct uart_litex_device_config *config = dev->config;
	int i;

	for (i = 0; i < size && !litex_read8(config->txfull_addr); i++) {
		litex_write8(tx_data[i], config->rxtx_addr);
	}

	return i;
}

/**
 * @brief Read data from FIFO
 *
 * @param dev UART device struct
 * @param rxData Data container
 * @param size Container size
 *
 * @return Number of bytes read
 */
static int uart_litex_fifo_read(const struct device *dev,
				uint8_t *rx_data, const int size)
{
	const struct uart_litex_device_config *config = dev->config;
	int i;

	for (i = 0; i < size && !litex_read8(config->rxempty_addr); i++) {
		rx_data[i] = litex_read8(config->rxtx_addr);

		/* refresh UART_RXEMPTY by writing UART_EV_RX
		 * to UART_EV_PENDING
		 */
		litex_write8(UART_EV_RX, config->ev_pending_addr);
	}

	return i;
}

static void uart_litex_irq_err(const struct device *dev)
{
	ARG_UNUSED(dev);
}

/**
 * @brief Check if any IRQ is pending
 *
 * @param dev UART device struct
 *
 * @return 1 if an IRQ is pending, 0 otherwise
 */
static int uart_litex_irq_is_pending(const struct device *dev)
{
	return (uart_litex_irq_tx_ready(dev) || uart_litex_irq_rx_ready(dev));
}

static int uart_litex_irq_update(const struct device *dev)
{
	return 1;
}

/**
 * @brief Set the callback function pointer for IRQ.
 *
 * @param dev UART device struct
 * @param cb Callback function pointer.
 */
static void uart_litex_irq_callback_set(const struct device *dev,
					uart_irq_callback_user_data_t cb,
					void *cb_data)
{
	struct uart_litex_data *data;

	data = dev->data;
	data->callback = cb;
	data->cb_data = cb_data;
}

static void uart_litex_irq_handler(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;
	struct uart_litex_data *data = dev->data;
	unsigned int key = irq_lock();

	if (data->callback) {
		data->callback(dev, data->cb_data);
	}

	// UART_EV_RX is cleared at point of use, do NOT clear here or the RX unit will hang!
	litex_write8(UART_EV_TX, config->ev_pending_addr);

	if ((litex_read8(config->ev_enable_addr) & UART_EV_TX) &&
		 (!litex_read8(config->txfull_addr))) {

		/*
		 * TX done event already generated an edge interrupt. Generate a
		 * soft interrupt and have it call the callback function in
		 * timer isr context.
		 */
		k_timer_start(&data->timer, K_NO_WAIT, K_NO_WAIT);
	}

	irq_unlock(key);
}

static void uart_litex_tx_soft_isr(struct k_timer *timer)
{
	const struct device *dev = k_timer_user_data_get(timer);

	uart_litex_irq_handler(dev);
}
#endif	/* CONFIG_UART_INTERRUPT_DRIVEN */

static const struct uart_driver_api uart_litex_driver_api = {
	.poll_in		= uart_litex_poll_in,
	.poll_out		= uart_litex_poll_out,
#ifdef CONFIG_UART_USE_RUNTIME_CONFIGURE
	.configure		= uart_litex_configure,
	.config_get		= uart_litex_config_get,
#endif
	.err_check		= NULL,
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	.fifo_fill		= uart_litex_fifo_fill,
	.fifo_read		= uart_litex_fifo_read,
	.irq_tx_enable		= uart_litex_irq_tx_enable,
	.irq_tx_disable		= uart_litex_irq_tx_disable,
	.irq_tx_ready		= uart_litex_irq_tx_ready,
	.irq_tx_complete	= uart_litex_irq_tx_complete,
	.irq_rx_enable		= uart_litex_irq_rx_enable,
	.irq_rx_disable		= uart_litex_irq_rx_disable,
	.irq_rx_ready		= uart_litex_irq_rx_ready,
	.irq_err_enable		= uart_litex_irq_err,
	.irq_err_disable	= uart_litex_irq_err,
	.irq_is_pending		= uart_litex_irq_is_pending,
	.irq_update		= uart_litex_irq_update,
	.irq_callback_set	= uart_litex_irq_callback_set
#endif
};

static int uart_litex_init(const struct device *dev)
{
	const struct uart_litex_device_config *config = dev->config;
	struct uart_litex_data *data = dev->data;

	if (config->phy_config_addr) {
		uint32_t phy_config = litex_read32(config->phy_config_addr);
		if (((phy_config >> PHY_CONFIG_STOP_BITS_SHIFT) & PHY_CONFIG_STOP_BITS_MASK) == 1) {
			data->uart_config.stop_bits = UART_CFG_STOP_BITS_2;
		}
		else {
			data->uart_config.stop_bits = UART_CFG_STOP_BITS_1;
		}
		switch ((phy_config >> PHY_CONFIG_DATA_BITS_SHIFT) & PHY_CONFIG_DATA_BITS_MASK) {
			case 0x5: data->uart_config.data_bits = UART_CFG_DATA_BITS_5;
			case 0x6: data->uart_config.data_bits = UART_CFG_DATA_BITS_6;
			case 0x7: data->uart_config.data_bits = UART_CFG_DATA_BITS_7;
			case 0x8: data->uart_config.data_bits = UART_CFG_DATA_BITS_8;
			default: data->uart_config.data_bits = 0;
		}
		switch ((phy_config >> PHY_CONFIG_PARITY_SHIFT) & PHY_CONFIG_PARITY_MASK) {
			case 0x0: data->uart_config.parity = UART_CFG_PARITY_NONE;
			case 0x2: data->uart_config.parity = UART_CFG_PARITY_EVEN;
			case 0x3: data->uart_config.parity = UART_CFG_PARITY_ODD;
			default: data->uart_config.parity = UART_CFG_PARITY_NONE;
		}
	}

	litex_write8(UART_EV_TX | UART_EV_RX, config->ev_pending_addr);

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	k_timer_init(&data->timer, &uart_litex_tx_soft_isr, NULL);
	k_timer_user_data_set(&data->timer, (void *)dev);

	config->config_func(dev);
#endif

	return 0;
}

#define LITEX_UART_IRQ_INIT(n)                                                                     \
	static void uart_irq_config##n(const struct device *dev)                                   \
	{                                                                                          \
		IRQ_CONNECT(DT_INST_IRQN(n), DT_INST_IRQ(n, priority), uart_litex_irq_handler,     \
			    DEVICE_DT_INST_GET(n), 0);                                             \
                                                                                                   \
		irq_enable(DT_INST_IRQN(n));                                                       \
	}

#define LITEX_UART_INIT(n)                                                                         \
	IF_ENABLED(CONFIG_UART_INTERRUPT_DRIVEN, (LITEX_UART_IRQ_INIT(n)))                         \
                                                                                                   \
	static struct uart_litex_data uart_litex_data_##n = {                                      \
		.uart_config.baudrate = DT_INST_PROP(n, current_speed),};                          \
                                                                                                   \
	static const struct uart_litex_device_config uart_litex_dev_cfg_##n = {                    \
		.rxtx_addr = DT_INST_REG_ADDR_BY_NAME(n, rxtx),                                    \
		.txfull_addr = DT_INST_REG_ADDR_BY_NAME(n, txfull),                                \
		.rxempty_addr = DT_INST_REG_ADDR_BY_NAME(n, rxempty),                              \
		.ev_status_addr = DT_INST_REG_ADDR_BY_NAME(n, ev_status),                          \
		.ev_pending_addr = DT_INST_REG_ADDR_BY_NAME(n, ev_pending),                        \
		.ev_enable_addr = DT_INST_REG_ADDR_BY_NAME(n, ev_enable),                          \
		.txempty_addr = DT_INST_REG_ADDR_BY_NAME(n, txempty),                              \
		.rxfull_addr = DT_INST_REG_ADDR_BY_NAME(n, rxfull),                                \
		IF_ENABLED(DT_INST_REG_HAS_NAME(n, phy_tuning), (.phy_tuning_addr =                \
			DT_INST_REG_ADDR_BY_NAME(n, phy_tuning),))                                 \
		IF_ENABLED(DT_INST_REG_HAS_NAME(n, phy_config), (.phy_config_addr =                \
			DT_INST_REG_ADDR_BY_NAME(n, phy_config),))                                 \
		IF_ENABLED(DT_INST_REG_HAS_NAME(n, bus_freq), (.bus_freq_addr =                    \
			DT_INST_REG_ADDR_BY_NAME(n, bus_freq),))                                   \
		IF_ENABLED(CONFIG_UART_INTERRUPT_DRIVEN, (.config_func = uart_irq_config##n,))};   \
                                                                                                   \
	DEVICE_DT_INST_DEFINE(n, uart_litex_init, NULL, &uart_litex_data_##n,                      \
			      &uart_litex_dev_cfg_##n, PRE_KERNEL_1, CONFIG_SERIAL_INIT_PRIORITY,  \
			      (void *)&uart_litex_driver_api);

DT_INST_FOREACH_STATUS_OKAY(LITEX_UART_INIT)

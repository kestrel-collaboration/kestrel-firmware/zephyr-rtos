/*
 * Copyright (c) 2024 Raptor Engineering, LLC <www.raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT ibm_builtin_darn

#include <zephyr/device.h>
#include <zephyr/drivers/entropy.h>
#include <errno.h>
#include <zephyr/init.h>
#include <string.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(entropy_openpower_darn, CONFIG_ENTROPY_LOG_LEVEL);

#define DARN_ERR 0XFFFFFFFFFFFFFFFFUL

static int entropy_openpower_get_random_number_darn(uint64_t* value)
{
	uint64_t val = DARN_ERR;

	__asm__ __volatile__ ("darn %0,1" : "=r"(val));
	if (val == DARN_ERR) {
		return -1;
	}

	*value = val;

	return 0;
}

static int entropy_openpower_get_entropy_darn(const struct device *dev, uint8_t *buffer,
					 uint16_t length)
{
	while (length > 0) {
		size_t to_copy;
		uint64_t value;

		if (entropy_openpower_get_random_number_darn(&value) < 0) {
			return -1;
		}
		to_copy = MIN(length, sizeof(value));

		memcpy(buffer, &value, to_copy);
		buffer += to_copy;
		length -= to_copy;
	}
	return 0;
}

static int entropy_openpower_init_darn(const struct device *dev)
{
	ARG_UNUSED(dev);

	int i;
	uint64_t value;
	for (i = 0; i < 10; i++) {
		if (entropy_openpower_get_random_number_darn(&value) == 0) {
			return 0;
		}
	}

	LOG_WRN("Builtin random number generator (darn) unusable, aborting!");

	return -1;
}

static const struct entropy_driver_api entropy_openpower_darn = {
	.get_entropy = entropy_openpower_get_entropy_darn
};

DEVICE_DT_INST_DEFINE(0, entropy_openpower_init_darn, NULL, NULL, NULL, PRE_KERNEL_1,
		      CONFIG_ENTROPY_INIT_PRIORITY, &entropy_openpower_darn);

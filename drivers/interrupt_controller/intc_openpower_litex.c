/*
 * Copyright (c) 2020-2024 Raptor Engineering, LLC <sales@raptorengineering.com>
 * Copyright (c) 2021 Evan Lojewski
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/arch/cpu.h>
#include <zephyr/init.h>
#include <zephyr/irq.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/kernel.h>
#include <zephyr/types.h>

// XICS/XICP peripheral information
#define XICSICS_BASE		DT_REG_ADDR(DT_INST(0, openpower_xics_sources))
#define XICSICP_BASE		DT_REG_ADDR(DT_INST(0, openpower_xics_presentation))

// XICS registers
#define PPC_XICS_XIRR_POLL	0x0
#define PPC_XICS_XIRR		0x4
#define PPC_XICS_RESV		0x8
#define PPC_XICS_MFRR		0xc

// Must match corresponding XICS ICS HDL parameter
#define PPC_XICS_SRC_NUM	16

// Default external interrupt priority set by software during IRQ enable
#define PPC_EXT_INTERRUPT_PRIO	0x08

#define KESTREL_SPR_KAISB    (850) /**< Kestrel architecture-specific Interrupt Vector Base register. */

#define EXCEPTION_MASK          (0xFFF) /**< Maximum valid exception address */
#define EXCEPTION_RESET         (0x100) /**< The causes of system reset exceptions are implementation-dependent. */
#define EXCEPTION_DSI           (0x300) /**< A DSI exception occurs when a data memory access cannot be performed. */
#define EXCEPTION_DSegI         (0x380) /**< A DSegI exception occurs when a data memory access cannot be performed due to a segment fault. */
#define EXCEPTION_ISI           (0x400) /**< An ISI exception occurs when an instruction fetch cannot be performed. */
#define EXCEPTION_ISegI         (0x480) /**< An ISegI exception occurs when an instruction fetch cannot be performed due to a segment fault. */
#define EXCEPTION_EXTERNAL_IRQ  (0x500) /**< An external interrupt is generated only when an external exception is pending and the interrupt is enabled (MSR[EE] = 1). */
#define EXCEPTION_PROGRAM       (0x700) /**< A program exception is caused by conditions which correspond to bit settings in SRR1 and arise during execution of an instruction. */
#define EXCEPTION_DECREMENTER   (0x900) /**< The exception is created when the most significant bit changes from 0 to 1. */
#define EXCEPTION_SYSTEM_CALL   (0xC00) /**< A system call exception occurs when a System Call (sc) instruction is executed. **/

#define bswap32(x) (uint32_t)__builtin_bswap32((uint32_t)(x))

// Address of exception / IRQ handler routine
extern void * __isr_address;
void xics_arch_generic_isr(uint64_t vec);

// Exception tracking
uint32_t irq_unhandled_vector = 0;
uint32_t irq_unhandled_source = 0;
uint8_t irq_unhandled_vector_valid = 0;
uint8_t irq_unhandled_source_valid = 0;

/**
 * Update the interupt vector table base address.
 *
 * @param ivt_base The 16K aligned interrupt vector table base address.
 */
void microwatt_irq_set_base(void *ivt_base)
{
	__asm__ volatile("mtspr %0, %1" : : "i" (KESTREL_SPR_KAISB), "r" (ivt_base) : "memory");
}

/**
 * Read the interupt vector table base address.
 *
 * @returns The 16K aligned interrupt vector table base address.
 */
void * microwatt_irq_get_base(void)
{
	void* ivt_base;

	__asm__ volatile("mfspr %0, %1" : "=r"(ivt_base) : "i" (KESTREL_SPR_KAISB) : "memory");

	return ivt_base;
}

// Related external functions
extern void powerpc_timebase_irq_handler(void *device);

static uint8_t xics_priority_table[PPC_XICS_SRC_NUM];

static inline uint8_t xics_icp_readb(int reg)
{
	return *((uint8_t*)(XICSICP_BASE + reg));
}

static inline void xics_icp_writeb(int reg, uint8_t value)
{
	*((uint8_t*)(XICSICP_BASE + reg)) = value;
}

static inline uint32_t xics_icp_readw(int reg)
{
	return bswap32(*((uint32_t*)(XICSICP_BASE + reg)));
}

static inline void xics_icp_writew(int reg, uint32_t value)
{
	*((uint32_t*)(XICSICP_BASE + reg)) = bswap32(value);
}

static inline uint32_t xics_ics_read_xive(int irq_number)
{
	return bswap32(*((uint32_t*)(XICSICS_BASE + 0x800 + (irq_number << 2))));
}

static inline void xics_ics_write_xive(int irq_number, uint32_t priority)
{
	*((uint32_t*)(XICSICS_BASE + 0x800 + (irq_number << 2))) = bswap32(priority);
}

static inline void mtdec(uint64_t val)
{
	__asm__ volatile("mtdec %0" : : "r" (val) : "memory");
}

static inline unsigned int irq_getmask(void)
{
	// Compute mask from enabled external interrupts in ICS
	uint32_t mask;
	int irq;
	mask = 0;
	for (irq = PPC_XICS_SRC_NUM - 1; irq >= 0; irq--) {
		mask = mask << 1;
		if ((xics_ics_read_xive(irq) & 0xff) != 0xff)
			mask |= 0x1;
	}
	return mask;
}

static inline void irq_setmask(unsigned int mask)
{
	int irq;

	// Enable all interrupts at a fixed priority level for now
	int priority_level = PPC_EXT_INTERRUPT_PRIO;

	// Iterate over IRQs configured in mask, and enable / mask in ICS
	for (irq = 0; irq < PPC_XICS_SRC_NUM; irq++) {
		if ((mask >> irq) & 0x1)
			xics_ics_write_xive(irq, priority_level);
		else
			xics_ics_write_xive(irq, 0xff);
	}
}

static inline unsigned int irq_pending(void)
{
	// Compute pending interrupt bitmask from asserted external interrupts in ICS
	uint32_t pending;
	int irq;
	pending = 0;
	for (irq = PPC_XICS_SRC_NUM - 1; irq >= 0; irq--) {
		pending = pending << 1;
		if ((xics_ics_read_xive(irq) & (0x1 << 31)) != 0)
			pending |= 0x1;
	}
	return pending;
}

static inline void isr_dec(void)
{
	//  For now, just set DEC back to a large enough value to slow the flood of
	//  DEC-initiated timer interrupts
	mtdec(0x000000000ffffff);
}

void xics_arch_generic_isr(uint64_t vec)
{
	vec = vec & 0xFFF;

	if (vec == 0x900)
	{
		// DEC interrupt
#ifdef CONFIG_POWERPC_TIMEBASE
		powerpc_timebase_irq_handler(NULL);
#else
		isr_dec();
#endif
		return;
	}

	if (vec == 0x500)
	{
		// Read interrupt source
		uint32_t xirr = xics_icp_readw(PPC_XICS_XIRR);
		uint32_t irq_source = xirr & 0x00ffffff;

		__attribute__((unused)) unsigned int irqs;
		__attribute__((unused)) struct _isr_table_entry *ite;

		// Handle IPI interrupts separately
		if (irq_source == 2)
		{
			// IPI interrupt
			xics_icp_writeb(PPC_XICS_MFRR, 0xff);
		}
		else if (irq_source == 0)
		{
			// Unknown source, slently ignore...
		}
		else
		{
			// External interrupt
			irqs = irq_pending() & irq_getmask();

			// Use the ISR table to call the registered handler for any assserted IRQ
			uint32_t irq_number;
			for (irq_number = 0; irq_number < CONFIG_NUM_IRQS; irq_number++) {
				if (irqs & (1 << irq_number)) {
					ite = &_sw_isr_table[irq_number];
					if ((void*)ite->isr == (void*)&z_irq_spurious) {
						printk("[WARNING] Spurious IRQ %d!\n", irq_number);
					}
					ite->isr(ite->arg);
				}
			}

			if (!irqs)
			{
				irq_unhandled_source = irq_source;
				irq_unhandled_source_valid = 1;
				z_irq_spurious((void*)((uintptr_t)irq_source));
			}
		}

		// Clear interrupt
		xics_icp_writew(PPC_XICS_XIRR, xirr);

		return;
	}

	irq_unhandled_vector = vec;
	irq_unhandled_vector_valid = 1;
	z_irq_spurious((void*)((uintptr_t)vec));
}

void xics_arch_irq_enable(unsigned int irq)
{
	xics_ics_write_xive(irq, xics_priority_table[irq]);
}

void xics_arch_irq_disable(unsigned int irq)
{
	xics_ics_write_xive(irq, 0xff);
}

int xics_arch_irq_is_enabled(unsigned int irq)
{
	if ((xics_ics_read_xive(irq) & 0xff) != 0xff)
	{
		return 1;
	}
	return 0;
}

void xics_arch_irq_priority_set(unsigned int irq, unsigned int prio, uint32_t flags)
{
	xics_priority_table[irq] = prio;
	if (xics_arch_irq_is_enabled(irq)) {
		xics_ics_write_xive(irq, xics_priority_table[irq]);
	}
}

static int xics_arch_irq_init(const struct device *dev)
{
	ARG_UNUSED(dev);

	// Initialize default interrupt priorities
	memset(xics_priority_table, PPC_EXT_INTERRUPT_PRIO, PPC_XICS_SRC_NUM);

	// Unmask all IRQs
	xics_icp_writeb(PPC_XICS_XIRR, 0xff);

	return 0;
}

SYS_INIT(xics_arch_irq_init, PRE_KERNEL_2, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);

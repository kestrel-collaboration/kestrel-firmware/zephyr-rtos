/*
 * Copyright (c) 2024 ANITRA system s.r.o.
 * Copyright (c) 2024 Raptor Engineering, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT epson_rx8900

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/rtc.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/util.h>
#include "rtc_utils.h"

LOG_MODULE_REGISTER(rx8900, CONFIG_RTC_LOG_LEVEL);

/* RX8900 RAM register addresses */
#define RX8900_REG_SECONDS              0x00
#define RX8900_REG_MINUTES              0x01
#define RX8900_REG_HOURS                0x02
#define RX8900_REG_WEEKDAY              0x03
#define RX8900_REG_DATE                 0x04
#define RX8900_REG_MONTH                0x05
#define RX8900_REG_YEAR                 0x06
#define RX8900_REG_ALARM_MINUTES        0x08
#define RX8900_REG_ALARM_HOURS          0x09
#define RX8900_REG_ALARM_WEEKDAY        0x0A
#define RX8900_REG_STATUS               0x0E
#define RX8900_REG_EXTENSION            0x0D
#define RX8900_REG_CONTROL              0x0F
#define RX8900_REG_USER_RAM1            0x07
#define RX8900_REG_BACKUP               0x18

#define RX8900_EXTENSION_WADA           BIT(6)

#define RX8900_CONTROL_AIE              BIT(3)
#define RX8900_CONTROL_UIE              BIT(5)

#define RX8900_STATUS_VLF               BIT(1)
#define RX8900_STATUS_AF                BIT(3)
#define RX8900_STATUS_UF                BIT(5)

#define RX8900_BACKUP_BSM               BIT(3)

#define RX8900_BSM_DISABLED             0x1
#define RX8900_BSM_BACKUP               0x0

#define RX8900_SECONDS_MASK             GENMASK(6, 0)
#define RX8900_MINUTES_MASK             GENMASK(6, 0)
#define RX8900_HOURS_AMPM               BIT(5)
#define RX8900_HOURS_12H_MASK           GENMASK(4, 0)
#define RX8900_HOURS_24H_MASK           GENMASK(5, 0)
#define RX8900_DATE_MASK                GENMASK(5, 0)
#define RX8900_WEEKDAY_MASK             GENMASK(6, 0)
#define RX8900_MONTH_MASK               GENMASK(4, 0)
#define RX8900_YEAR_MASK                GENMASK(7, 0)

#define RX8900_ALARM_MINUTES_AE_M       BIT(7)
#define RX8900_ALARM_MINUTES_MASK       GENMASK(6, 0)
#define RX8900_ALARM_HOURS_AE_H         BIT(7)
#define RX8900_ALARM_HOURS_AMPM         BIT(5)
#define RX8900_ALARM_HOURS_12H_MASK     GENMASK(4, 0)
#define RX8900_ALARM_HOURS_24H_MASK     GENMASK(5, 0)
#define RX8900_ALARM_DATE_AE_WD         BIT(7)
#define RX8900_ALARM_DATE_MASK          GENMASK(5, 0)

/* The RX8900 only supports two-digit years. Leap years are correctly handled from 2000 to 2099 */
#define RX8900_YEAR_OFFSET              (2000 - 1900)

/* The RX8900 enumerates months 1 to 12 */
#define RX8900_MONTH_OFFSET 1

/* RTC alarm time fields supported by the RX8900 */
#define RX8900_RTC_ALARM_TIME_MASK                                                                 \
	(RTC_ALARM_TIME_MASK_MINUTE | RTC_ALARM_TIME_MASK_HOUR | RTC_ALARM_TIME_MASK_MONTHDAY)

/* RTC time fields supported by the RX8900 */
#define RX8900_RTC_TIME_MASK                                                                       \
	(RTC_ALARM_TIME_MASK_SECOND | RTC_ALARM_TIME_MASK_MINUTE | RTC_ALARM_TIME_MASK_HOUR |      \
	 RTC_ALARM_TIME_MASK_MONTH | RTC_ALARM_TIME_MASK_MONTHDAY | RTC_ALARM_TIME_MASK_YEAR |     \
	 RTC_ALARM_TIME_MASK_WEEKDAY)

/* Helper macro to guard int-gpios related code */
#if DT_ANY_INST_HAS_PROP_STATUS_OKAY(int_gpios) &&                                                 \
	(defined(CONFIG_RTC_ALARM) || defined(CONFIG_RTC_UPDATE))
#define RX8900_INT_GPIOS_IN_USE 1
#endif

struct rx8900_config {
	const struct i2c_dt_spec i2c;
#ifdef RX8900_INT_GPIOS_IN_USE
	struct gpio_dt_spec gpio_int;
#endif /* RX8900_INT_GPIOS_IN_USE */
	uint8_t backup;
};

struct rx8900_data {
	struct k_sem lock;
#if RX8900_INT_GPIOS_IN_USE
	const struct device *dev;
	struct gpio_callback int_callback;
	struct k_work work;

#ifdef CONFIG_RTC_ALARM
	rtc_alarm_callback alarm_callback;
	void *alarm_user_data;
#endif /* CONFIG_RTC_ALARM */
#ifdef CONFIG_RTC_UPDATE
	rtc_update_callback update_callback;
	void *update_user_data;
#endif /* CONFIG_RTC_UPDATE */
#endif /* RX8900_INT_GPIOS_IN_USE */
};

static uint8_t rx8900_day_number_to_day_of_week_code(int weekday)
{
	return 0x1 << weekday;
}

static int rx8900_day_of_week_code_to_day_number(uint8_t week_code)
{
	switch (week_code)
	{
		case 0x01: return 0;
		case 0x02: return 1;
		case 0x04: return 2;
		case 0x08: return 3;
		case 0x10: return 4;
		case 0x20: return 5;
		case 0x40: return 6;
		default: return 0;
	}
}

static void rx8900_lock_sem(const struct device *dev)
{
	struct rx8900_data *data = dev->data;

	(void)k_sem_take(&data->lock, K_FOREVER);
}

static void rx8900_unlock_sem(const struct device *dev)
{
	struct rx8900_data *data = dev->data;

	k_sem_give(&data->lock);
}

static int rx8900_read_regs(const struct device *dev, uint8_t addr, void *buf, size_t len)
{
	const struct rx8900_config *config = dev->config;
	int err;

	err = i2c_write_read_dt(&config->i2c, &addr, sizeof(addr), buf, len);
	if (err) {
		LOG_ERR("failed to read reg addr 0x%02x, len %ld (err %d)", addr, len, err);
		return err;
	}

	return 0;
}

static int rx8900_read_reg8(const struct device *dev, uint8_t addr, uint8_t *val)
{
	return rx8900_read_regs(dev, addr, val, sizeof(*val));
}

static int rx8900_write_regs(const struct device *dev, uint8_t addr, void *buf, size_t len)
{
	const struct rx8900_config *config = dev->config;
	uint8_t block[sizeof(addr) + len];
	int err;

	block[0] = addr;
	memcpy(&block[1], buf, len);

	err = i2c_write_dt(&config->i2c, block, sizeof(block));
	if (err) {
		LOG_ERR("failed to write reg addr 0x%02x, len %ld (err %d)", addr, len, err);
		return err;
	}

	return 0;
}

static int rx8900_write_reg8(const struct device *dev, uint8_t addr, uint8_t val)
{
	return rx8900_write_regs(dev, addr, &val, sizeof(val));
}

static int rx8900_update_reg8(const struct device *dev, uint8_t addr, uint8_t mask, uint8_t val)
{
	const struct rx8900_config *config = dev->config;
	int err;

	err = i2c_reg_update_byte_dt(&config->i2c, addr, mask, val);
	if (err) {
		LOG_ERR("failed to update reg addr 0x%02x, mask 0x%02x, val 0x%02x (err %d)", addr,
			mask, val, err);
		return err;
	}

	return 0;
}

#if RX8900_INT_GPIOS_IN_USE

static int rx8900_int_enable_unlocked(const struct device *dev, bool enable)
{
	const struct rx8900_config *config = dev->config;
	uint8_t clkout = 0;
	int err;

	err = gpio_pin_interrupt_configure_dt(&config->gpio_int,
					      enable ? GPIO_INT_EDGE_TO_ACTIVE : GPIO_INT_DISABLE);
	if (err) {
		LOG_ERR("failed to %s GPIO interrupt (err %d)", enable ? "enable" : "disable", err);
		return err;
	}

	return 0;
}

static void rx8900_work_cb(struct k_work *work)
{
	struct rx8900_data *data = CONTAINER_OF(work, struct rx8900_data, work);
	const struct device *dev = data->dev;
	rtc_alarm_callback alarm_callback = NULL;
	void *alarm_user_data = NULL;
	rtc_update_callback update_callback = NULL;
	void *update_user_data = NULL;
	uint8_t status;
	int err;

	rx8900_lock_sem(dev);

	err = rx8900_read_reg8(data->dev, RX8900_REG_STATUS, &status);
	if (err) {
		goto unlock;
	}

#ifdef CONFIG_RTC_ALARM
	if ((status & RX8900_STATUS_AF) && data->alarm_callback != NULL) {
		status &= ~(RX8900_STATUS_AF);
		alarm_callback = data->alarm_callback;
		alarm_user_data = data->alarm_user_data;
	}
#endif /* CONFIG_RTC_ALARM */

#ifdef CONFIG_RTC_UPDATE
	if ((status & RX8900_STATUS_UF) && data->update_callback != NULL) {
		status &= ~(RX8900_STATUS_UF);
		update_callback = data->update_callback;
		update_user_data = data->update_user_data;
	}
#endif /* CONFIG_RTC_UPDATE */

	err = rx8900_write_reg8(dev, RX8900_REG_STATUS, status);
	if (err) {
		goto unlock;
	}

	/* Check if interrupt occurred between STATUS read/write */
	err = rx8900_read_reg8(dev, RX8900_REG_STATUS, &status);
	if (err) {
		goto unlock;
	}

	if (((status & RX8900_STATUS_AF) && alarm_callback != NULL) ||
	    ((status & RX8900_STATUS_UF) && update_callback != NULL)) {
		/* Another interrupt occurred while servicing this one */
		k_work_submit(&data->work);
	}

unlock:
	rx8900_unlock_sem(dev);

	if (alarm_callback != NULL) {
		alarm_callback(dev, 0U, alarm_user_data);
		alarm_callback = NULL;
	}

	if (update_callback != NULL) {
		update_callback(dev, update_user_data);
		update_callback = NULL;
	}
}

static void rx8900_int_handler(const struct device *port, struct gpio_callback *cb,
			       gpio_port_pins_t pins)
{
	struct rx8900_data *data = CONTAINER_OF(cb, struct rx8900_data, int_callback);

	ARG_UNUSED(port);
	ARG_UNUSED(pins);

	k_work_submit(&data->work);
}

#endif /* RX8900_INT_GPIOS_IN_USE */

static int rx8900_set_time(const struct device *dev, const struct rtc_time *timeptr)
{
	uint8_t date[7];
	uint8_t status;
	int err;

	if (timeptr == NULL ||
	    !rtc_utils_validate_rtc_time(timeptr, RX8900_RTC_TIME_MASK) ||
	    (timeptr->tm_year < RX8900_YEAR_OFFSET)) {
		LOG_ERR("invalid time");
		return -EINVAL;
	}

	rx8900_lock_sem(dev);

	LOG_DBG("set time: year = %d, mon = %d, mday = %d, wday = %d, hour = %d, "
		"min = %d, sec = %d",
		timeptr->tm_year, timeptr->tm_mon, timeptr->tm_mday, timeptr->tm_wday,
		timeptr->tm_hour, timeptr->tm_min, timeptr->tm_sec);

	date[0] = bin2bcd(timeptr->tm_sec) & RX8900_SECONDS_MASK;
	date[1] = bin2bcd(timeptr->tm_min) & RX8900_MINUTES_MASK;
	date[2] = bin2bcd(timeptr->tm_hour) & RX8900_HOURS_24H_MASK;
	date[3] = rx8900_day_number_to_day_of_week_code(timeptr->tm_wday) & RX8900_WEEKDAY_MASK;
	date[4] = bin2bcd(timeptr->tm_mday) & RX8900_DATE_MASK;
	date[5] = bin2bcd(timeptr->tm_mon + RX8900_MONTH_OFFSET) & RX8900_MONTH_MASK;
	date[6] = bin2bcd(timeptr->tm_year - RX8900_YEAR_OFFSET) & RX8900_YEAR_MASK;

	LOG_DBG("raw data 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x",
		date[0], date[1], date[2], date[3], date[4], date[5], date[6]);

	err = rx8900_write_regs(dev, RX8900_REG_SECONDS, &date, sizeof(date));
	if (err) {
		goto unlock;
	}

	/* Read status */
	err = rx8900_read_reg8(dev, RX8900_REG_STATUS, &status);
	if (err) {
		return err;
	}

	LOG_DBG("Status register: 0x%02x", status);

	if (status & RX8900_STATUS_VLF) {
		LOG_DBG("Undervoltage detected, clearing flag");

		/* Clear Voltage Low Flag */
		err = rx8900_update_reg8(dev, RX8900_REG_STATUS, RX8900_STATUS_VLF, 0);
	}

unlock:
	rx8900_unlock_sem(dev);

	return err;
}

static int rx8900_get_time(const struct device *dev, struct rtc_time *timeptr)
{
	uint8_t status;
	uint8_t date[7];
	int err;

	if (timeptr == NULL) {
		return -EINVAL;
	}

	err = rx8900_read_reg8(dev, RX8900_REG_STATUS, &status);
	if (err) {
		return err;
	}

	LOG_DBG("Status register: 0x%02x", status);

	if (status & RX8900_STATUS_VLF) {
		/* Voltage Low Flag indicates invalid data */
		return -ENODATA;
	}

	err = rx8900_read_regs(dev, RX8900_REG_SECONDS, date, sizeof(date));
	if (err) {
		return err;
	}

	LOG_DBG("raw data 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x",
		date[0], date[1], date[2], date[3], date[4], date[5], date[6]);

	memset(timeptr, 0U, sizeof(*timeptr));
	timeptr->tm_sec = bcd2bin(date[0] & RX8900_SECONDS_MASK);
	timeptr->tm_min = bcd2bin(date[1] & RX8900_MINUTES_MASK);
	timeptr->tm_hour = bcd2bin(date[2] & RX8900_HOURS_24H_MASK);
	timeptr->tm_wday = rx8900_day_of_week_code_to_day_number(date[3] & RX8900_WEEKDAY_MASK);
	timeptr->tm_mday = bcd2bin(date[4] & RX8900_DATE_MASK);
	timeptr->tm_mon = bcd2bin(date[5] & RX8900_MONTH_MASK) - RX8900_MONTH_OFFSET;
	timeptr->tm_year = bcd2bin(date[6] & RX8900_YEAR_MASK) + RX8900_YEAR_OFFSET;
	timeptr->tm_yday = -1;
	timeptr->tm_isdst = -1;

	LOG_DBG("get time: year = %d, mon = %d, mday = %d, wday = %d, hour = %d, "
		"min = %d, sec = %d",
		timeptr->tm_year, timeptr->tm_mon, timeptr->tm_mday, timeptr->tm_wday,
		timeptr->tm_hour, timeptr->tm_min, timeptr->tm_sec);

	return 0;
}

#ifdef CONFIG_RTC_ALARM

static int rx8900_alarm_get_supported_fields(const struct device *dev, uint16_t id, uint16_t *mask)
{
	ARG_UNUSED(dev);

	if (id != 0U) {
		LOG_ERR("invalid alarm ID %d", id);
		return -EINVAL;
	}

	*mask = RX8900_RTC_ALARM_TIME_MASK;

	return 0;
}

static int rx8900_alarm_set_time(const struct device *dev, uint16_t id, uint16_t mask,
				 const struct rtc_time *timeptr)
{
	uint8_t regs[3];

	if (id != 0U) {
		LOG_ERR("invalid alarm ID %d", id);
		return -EINVAL;
	}

	if (mask & ~(RX8900_RTC_ALARM_TIME_MASK)) {
		LOG_ERR("unsupported alarm field mask 0x%04x", mask);
		return -EINVAL;
	}

	if (!rtc_utils_validate_rtc_time(timeptr, mask)) {
		LOG_ERR("invalid alarm time");
		return -EINVAL;
	}

	if (mask & RTC_ALARM_TIME_MASK_MINUTE) {
		regs[0] = bin2bcd(timeptr->tm_min) & RX8900_ALARM_MINUTES_MASK;
	} else {
		regs[0] = RTC_ALARM_TIME_MASK_MINUTE;
	}

	if (mask & RTC_ALARM_TIME_MASK_HOUR) {
		regs[1] = bin2bcd(timeptr->tm_hour) & RX8900_ALARM_HOURS_24H_MASK;
	} else {
		regs[1] = RX8900_ALARM_HOURS_AE_H;
	}

	if (mask & RTC_ALARM_TIME_MASK_MONTHDAY) {
		regs[2] = bin2bcd(timeptr->tm_mday) & RX8900_ALARM_DATE_MASK;
	} else {
		regs[2] = RX8900_ALARM_DATE_AE_WD;
	}

	LOG_DBG("set alarm: mday = %d, hour = %d, min = %d, mask = 0x%04x",
		timeptr->tm_mday, timeptr->tm_hour, timeptr->tm_min, mask);

	/* Write registers RX8900_REG_ALARM_MINUTES through RX8900_REG_ALARM_WEEKDAY */
	return rx8900_write_regs(dev, RX8900_REG_ALARM_MINUTES, &regs, sizeof(regs));
}

static int rx8900_alarm_get_time(const struct device *dev, uint16_t id, uint16_t *mask,
				 struct rtc_time *timeptr)
{
	uint8_t regs[3];
	int err;

	if (id != 0U) {
		LOG_ERR("invalid alarm ID %d", id);
		return -EINVAL;
	}

	/* Read registers RX8900_REG_ALARM_MINUTES through RX8900_REG_ALARM_WEEKDAY */
	err = rx8900_read_regs(dev, RX8900_REG_ALARM_MINUTES, &regs, sizeof(regs));
	if (err) {
		return err;
	}

	memset(timeptr, 0U, sizeof(*timeptr));
	*mask = 0U;

	if ((regs[0] & RX8900_ALARM_MINUTES_AE_M) == 0) {
		timeptr->tm_min = bcd2bin(regs[0] & RX8900_ALARM_MINUTES_MASK);
		*mask |= RTC_ALARM_TIME_MASK_MINUTE;
	}

	if ((regs[1] & RX8900_ALARM_HOURS_AE_H) == 0) {
		timeptr->tm_hour = bcd2bin(regs[1] & RX8900_ALARM_HOURS_24H_MASK);
		*mask |= RTC_ALARM_TIME_MASK_HOUR;
	}

	if ((regs[2] & RX8900_ALARM_DATE_AE_WD) == 0) {
		timeptr->tm_mday = bcd2bin(regs[2] & RX8900_ALARM_DATE_MASK);
		*mask |= RTC_ALARM_TIME_MASK_MONTHDAY;
	}

	LOG_DBG("get alarm: mday = %d, hour = %d, min = %d, mask = 0x%04x",
		timeptr->tm_mday, timeptr->tm_hour, timeptr->tm_min, *mask);

	return 0;
}

static int rx8900_alarm_is_pending(const struct device *dev, uint16_t id)
{
	uint8_t status;
	int err;

	if (id != 0U) {
		LOG_ERR("invalid alarm ID %d", id);
		return -EINVAL;
	}

	rx8900_lock_sem(dev);

	err = rx8900_read_reg8(dev, RX8900_REG_STATUS, &status);
	if (err) {
		goto unlock;
	}

	if (status & RX8900_STATUS_AF) {
		/* Clear alarm flag */
		status &= ~(RX8900_STATUS_AF);

		err = rx8900_write_reg8(dev, RX8900_REG_STATUS, status);
		if (err) {
			goto unlock;
		}

		/* Alarm pending */
		err = 1;
	}

unlock:
	rx8900_unlock_sem(dev);

	return err;
}

static int rx8900_alarm_set_callback(const struct device *dev, uint16_t id,
				     rtc_alarm_callback callback, void *user_data)
{
#ifndef RX8900_INT_GPIOS_IN_USE
	ARG_UNUSED(dev);
	ARG_UNUSED(id);
	ARG_UNUSED(callback);
	ARG_UNUSED(user_data);

	return -ENOTSUP;
#else
	const struct rx8900_config *config = dev->config;
	struct rx8900_data *data = dev->data;
	uint8_t control_2;
	int err = 0;

	if (config->gpio_int.port == NULL) {
		return -ENOTSUP;
	}

	if (id != 0U) {
		LOG_ERR("invalid alarm ID %d", id);
		return -EINVAL;
	}

	rx8900_lock_sem(dev);

	data->alarm_callback = callback;
	data->alarm_user_data = user_data;

	err = rx8900_read_reg8(dev, RX8900_REG_CONTROL, &control_2);
	if (err) {
		goto unlock;
	}

	if (callback != NULL) {
		control_2 |= RX8900_CONTROL_AIE;
	} else {
		control_2 &= ~(RX8900_CONTROL_AIE);
	}

	if ((control_2 & RX8900_CONTROL_UIE) == 0U) {
		/* Only change INT GPIO if periodic time update interrupt not enabled */
		err = rx8900_int_enable_unlocked(dev, callback != NULL);
		if (err) {
			goto unlock;
		}
	}

	err = rx8900_write_reg8(dev, RX8900_REG_CONTROL, control_2);
	if (err) {
		goto unlock;
	}

unlock:
	rx8900_unlock_sem(dev);

	/* Alarm flag may already be set */
	k_work_submit(&data->work);

	return err;
#endif /* RX8900_INT_GPIOS_IN_USE */
}

#endif /* CONFIG_RTC_ALARM */

#if RX8900_INT_GPIOS_IN_USE && defined(CONFIG_RTC_UPDATE)

static int rx8900_update_set_callback(const struct device *dev, rtc_update_callback callback,
				      void *user_data)
{
	const struct rx8900_config *config = dev->config;
	struct rx8900_data *data = dev->data;
	uint8_t control_2;
	int err;

	if (config->gpio_int.port == NULL) {
		return -ENOTSUP;
	}

	rx8900_lock_sem(dev);

	data->update_callback = callback;
	data->update_user_data = user_data;

	err = rx8900_read_reg8(dev, RX8900_REG_CONTROL, &control_2);
	if (err) {
		goto unlock;
	}

	if (callback != NULL) {
		control_2 |= RX8900_CONTROL_UIE;
	} else {
		control_2 &= ~(RX8900_CONTROL_UIE);
	}

	if ((control_2 & RX8900_CONTROL_AIE) == 0U) {
		/* Only change INT GPIO if alarm interrupt not enabled */
		err = rx8900_int_enable_unlocked(dev, callback != NULL);
		if (err) {
			goto unlock;
		}
	}

	err = rx8900_write_reg8(dev, RX8900_REG_CONTROL, control_2);
	if (err) {
		goto unlock;
	}

unlock:
	rx8900_unlock_sem(dev);

	/* Seconds flag may already be set */
	k_work_submit(&data->work);

	return err;
}

#endif /* RX8900_INT_GPIOS_IN_USE && defined(CONFIG_RTC_UPDATE) */

static int rx8900_init(const struct device *dev)
{
	const struct rx8900_config *config = dev->config;
	struct rx8900_data *data = dev->data;
	uint8_t regs[3];
	uint8_t val;
	int err;

	k_sem_init(&data->lock, 1, 1);

	if (!i2c_is_ready_dt(&config->i2c)) {
		LOG_ERR("I2C bus not ready");
		return -ENODEV;
	}

#if RX8900_INT_GPIOS_IN_USE
	if (config->gpio_int.port != NULL) {
		if (!gpio_is_ready_dt(&config->gpio_int)) {
			LOG_ERR("GPIO not ready");
			return -ENODEV;
		}

		err = gpio_pin_configure_dt(&config->gpio_int, GPIO_INPUT);
		if (err) {
			LOG_ERR("failed to configure GPIO (err %d)", err);
			return -ENODEV;
		}

		gpio_init_callback(&data->int_callback, rx8900_int_handler,
				   BIT(config->gpio_int.pin));

		err = gpio_add_callback_dt(&config->gpio_int, &data->int_callback);
		if (err) {
			LOG_ERR("failed to add GPIO callback (err %d)", err);
			return -ENODEV;
		}

		data->dev = dev;
		data->work.handler = rx8900_work_cb;
	}
#endif /* RX8900_INT_GPIOS_IN_USE */

	err = rx8900_read_reg8(dev, RX8900_REG_STATUS, &val);
	if (err) {
		return -ENODEV;
	}

	if (val & RX8900_STATUS_AF) {
		LOG_WRN("an alarm may have been missed");
	}

	err = rx8900_update_reg8(dev, RX8900_REG_BACKUP, RX8900_BACKUP_BSM,
				config->backup);
	if (err) {
		return -ENODEV;
	}

	err = rx8900_update_reg8(dev, RX8900_REG_EXTENSION, RX8900_EXTENSION_WADA,
				 RX8900_EXTENSION_WADA);
	if (err) {
		return -ENODEV;
	}

	/* Disable the alarms */
	err = rx8900_update_reg8(dev,
				 RX8900_REG_CONTROL,
				 RX8900_CONTROL_AIE | RX8900_CONTROL_UIE,
				 0);
	if (err) {
		return -ENODEV;
	}

	err = rx8900_read_regs(dev, RX8900_REG_ALARM_MINUTES, regs, sizeof(regs));
	if (err) {
		return -ENODEV;
	}

	regs[0] |= RX8900_ALARM_MINUTES_AE_M;
	regs[1] |= RX8900_ALARM_HOURS_AE_H;
	regs[2] |= RX8900_ALARM_DATE_AE_WD;

	err = rx8900_write_regs(dev, RX8900_REG_ALARM_MINUTES, regs, sizeof(regs));
	if (err) {
		return -ENODEV;
	}

	return 0;
}

static const struct rtc_driver_api rx8900_driver_api = {
	.set_time = rx8900_set_time,
	.get_time = rx8900_get_time,
#ifdef CONFIG_RTC_ALARM
	.alarm_get_supported_fields = rx8900_alarm_get_supported_fields,
	.alarm_set_time = rx8900_alarm_set_time,
	.alarm_get_time = rx8900_alarm_get_time,
	.alarm_is_pending = rx8900_alarm_is_pending,
	.alarm_set_callback = rx8900_alarm_set_callback,
#endif /* CONFIG_RTC_ALARM */
#if RX8900_INT_GPIOS_IN_USE && defined(CONFIG_RTC_UPDATE)
	.update_set_callback = rx8900_update_set_callback,
#endif /* RX8900_INT_GPIOS_IN_USE && defined(CONFIG_RTC_UPDATE) */
};

#define RX8900_BSM_FROM_DT_INST(inst)                                                              \
	UTIL_CAT(RX8900_BSM_, DT_INST_STRING_UPPER_TOKEN(inst, backup_switch_mode))

#define RX8900_BACKUP_FROM_DT_INST(inst)                                                           \
	((FIELD_PREP(RX8900_BACKUP_BSM, RX8900_BSM_FROM_DT_INST(inst))))

#define RX8900_INIT(inst)                                                                          \
	static const struct rx8900_config rx8900_config_##inst = {                                 \
		.i2c = I2C_DT_SPEC_INST_GET(inst),                                                 \
		.backup = RX8900_BACKUP_FROM_DT_INST(inst),                                        \
		IF_ENABLED(RX8900_INT_GPIOS_IN_USE,                                                \
			   (.gpio_int = GPIO_DT_SPEC_INST_GET_OR(inst, int_gpios, {0})))};         \
                                                                                                   \
	static struct rx8900_data rx8900_data_##inst;                                              \
                                                                                                   \
	DEVICE_DT_INST_DEFINE(inst, &rx8900_init, NULL, &rx8900_data_##inst,                       \
			      &rx8900_config_##inst, POST_KERNEL, CONFIG_RTC_INIT_PRIORITY,        \
			      &rx8900_driver_api);

DT_INST_FOREACH_STATUS_OKAY(RX8900_INIT)

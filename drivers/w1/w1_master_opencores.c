/*
 * Copyright (c) 2020-2024 Raptor Engineering, LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT zephyr_w1_master_opencores

/**
 * @brief 1-Wire Bus Master driver using OpenCores hardware bus master.
 *
 * This file contains the implementation of the 1-Wire Bus Master driver using
 * an OpenCores hardware bus master, for example the bus masters used by the Kestrel
 * OpenPOWER BMC.
 */

#include <zephyr/drivers/w1.h>
#include <zephyr/device.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(w1_opencores_master, CONFIG_W1_LOG_LEVEL);

#include "w1_master_opencores.h"

struct w1_opencores_master_config {
	/** w1 master config, common to all drivers */
	struct w1_master_config master_config;
	/** Bus master used for 1-Wire communication */
	uintptr_t base_address;
	/** Peripheral core clock in MHz */
	uint32_t core_clock_frequency;
};

struct w1_opencores_master_data {
	/** w1 master data, common to all drivers */
	struct w1_master_data master_data;
	/** timing parameters for 1-Wire communication */
	const struct w1_opencores_master_timing *timing;
	/** overdrive speed mode active */
	bool overdrive_active;
};

static int w1_opencores_transfer_onewire_bit(uintptr_t base_address, uint8_t enable_overdrive, uint8_t tx_data, uint8_t *rx_data) {
	uint32_t onewire_op_timeout_counter;
	uint8_t onewire_op_failed;

	// Send and receive a single bit
	onewire_op_timeout_counter = 0;
	onewire_op_failed = 0;
	*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) =
		(OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_MASK << OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_SHIFT) |
		(enable_overdrive & (OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_MASK << OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_SHIFT)) |
		((!!tx_data) & (OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_MASK << OPENCORES_1WIRE_MASTER_CTL_DAT_TX_REQ_SHIFT));

	// Wait for cycle completion
	while ((*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_SHIFT) &
		OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_MASK) {
		if (onewire_op_timeout_counter > ONEWIRE_MASTER_OPERATION_TIMEOUT_VALUE) {
			LOG_WRN("1-wire operation timed out in bus transaction!");
			onewire_op_failed = 1;
			break;
		}
		k_usleep(10);
		onewire_op_timeout_counter++;
	}

	// Return received bit if timeout not hit
	if (!onewire_op_failed) {
		if (rx_data) {
			*rx_data = (*((volatile uint8_t *)(base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_SHIFT) &
				OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_MASK;
		}
	}

	if (onewire_op_failed) {
		return -1;
	}

	return 0;
}

static int w1_opencores_transfer_onewire_byte(uintptr_t base_address, uint8_t enable_overdrive, uint8_t tx_data, uint8_t *rx_data) {
	uint8_t onewire_op_failed;
	uint8_t rx_byte;
	uint8_t byte;
	int i;

	// Send and receive a single byte
	onewire_op_failed = 0;
	rx_byte = 0;
	for (i=0; i<8; i++) {
		if (w1_opencores_transfer_onewire_bit(base_address, enable_overdrive, (tx_data >> i) & 0x1, &byte))
		{
			onewire_op_failed = 1;
			break;
		}
		rx_byte |= (byte & 0x1) << i;
	}

	// Return received byte if timeout not hit
	if (!onewire_op_failed) {
		if (rx_data) {
			*rx_data = rx_byte;
		}
	}

	if (onewire_op_failed) {
		return -1;
	}

	return 0;
}

static int w1_opencores_master_reset_bus(const struct device *dev)
{
	const struct w1_opencores_master_config *cfg = dev->config;
	struct w1_opencores_master_data *data = dev->data;

	uint32_t onewire_op_timeout_counter;
	uint8_t onewire_op_failed;

	// Send reset
	onewire_op_timeout_counter = 0;
	onewire_op_failed = 0;
	*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) =
		(OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_MASK << OPENCORES_1WIRE_MASTER_CTL_CYCLE_START_SHIFT) |
		(data->overdrive_active & (OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_MASK << OPENCORES_1WIRE_MASTER_CTL_OVERDR_EN_SHIFT)) |
		(OPENCORES_1WIRE_MASTER_CTL_RST_REQ_MASK << OPENCORES_1WIRE_MASTER_CTL_RST_REQ_SHIFT);

	// Wait for cycle completion
	while ((*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_SHIFT) &
		OPENCORES_1WIRE_MASTER_CTL_CYCLE_STATUS_MASK) {
		if (onewire_op_timeout_counter > ONEWIRE_MASTER_OPERATION_TIMEOUT_VALUE) {
			LOG_WRN("1-wire operation timed out in bus reset!");
			onewire_op_failed = 1;
			break;
		}
		k_usleep(10);
		onewire_op_timeout_counter++;
	}

	// Return received bit if timeout not hit
	if (!onewire_op_failed) {
		return !((*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_BUS_RDT_CTL_STA)) >> OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_SHIFT) &
				OPENCORES_1WIRE_MASTER_CTL_DAT_RX_STA_MASK);
	}

	if (onewire_op_failed) {
		return -1;
	}

	return 0;
}

static int w1_opencores_master_read_bit(const struct device *dev)
{
	const struct w1_opencores_master_config *cfg = dev->config;
	struct w1_opencores_master_data *data = dev->data;

	int ret = 0;
	uint8_t rx_data = 0;

	ret = w1_opencores_transfer_onewire_bit(cfg->base_address, data->overdrive_active, 1, &rx_data);
	if (ret == 0) {
		return rx_data;
	}

	return -1;
}

static int w1_opencores_master_write_bit(const struct device *dev, const bool bit)
{
	const struct w1_opencores_master_config *cfg = dev->config;
	struct w1_opencores_master_data *data = dev->data;

	return w1_opencores_transfer_onewire_bit(cfg->base_address, data->overdrive_active, bit, NULL);
}

static int w1_opencores_master_read_byte(const struct device *dev)
{
	const struct w1_opencores_master_config *cfg = dev->config;
	struct w1_opencores_master_data *data = dev->data;

	int ret = 0;
	uint8_t rx_data = 0;

	ret = w1_opencores_transfer_onewire_byte(cfg->base_address, data->overdrive_active, 0xff, &rx_data);
	if (ret == 0) {
		return rx_data;
	}

	return -1;
}

static int w1_opencores_master_write_byte(const struct device *dev, const uint8_t byte)
{
	const struct w1_opencores_master_config *cfg = dev->config;
	struct w1_opencores_master_data *data = dev->data;

	return w1_opencores_transfer_onewire_byte(cfg->base_address, data->overdrive_active, byte, NULL);
}

static int w1_opencores_master_configure(const struct device *dev, enum w1_settings_type type, uint32_t value)
{
	struct w1_opencores_master_data *data = dev->data;

	switch (type) {
	case W1_SETTING_SPEED:
		data->overdrive_active = (value != 0);
		return 0;
	default:
		return -ENOTSUP;
	}
}

static int w1_opencores_master_init(const struct device *dev)
{
	const struct w1_opencores_master_config *cfg = dev->config;
	struct w1_opencores_master_data *data = dev->data;

	data->overdrive_active = false;

	LOG_DBG("Configuring 1-wire master at address %p...", (void*)cfg->base_address);

	if ((*((volatile uint32_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_DEVICE_ID_HIGH)) != OPENCORES_1WIRE_DEVICE_ID_HIGH) ||
		(*((volatile uint32_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_DEVICE_ID_LOW)) != OPENCORES_1WIRE_DEVICE_ID_LOW)) {
		return -1;
	}
	uint32_t opencores_onewire_version = *((volatile uint32_t *)(cfg->base_address +  OPENCORES_1WIRE_MASTER_DEVICE_VERSION));
	LOG_DBG("OpenCores 1-wire master found, device version %0d.%0d.%d",
		(opencores_onewire_version >>  OPENCORES_1WIRE_VERSION_MAJOR_SHIFT) &  OPENCORES_1WIRE_VERSION_MAJOR_MASK,
		(opencores_onewire_version >>  OPENCORES_1WIRE_VERSION_MINOR_SHIFT) &  OPENCORES_1WIRE_VERSION_MINOR_MASK,
		(opencores_onewire_version >>  OPENCORES_1WIRE_VERSION_PATCH_SHIFT) &  OPENCORES_1WIRE_VERSION_PATCH_MASK);

	// Compute prescale value from system clock and 1-wire bus specifications
	// Overdrive is 1us, normal is 5us...
	uint16_t normal_prescale = ((cfg->core_clock_frequency * 5) / 1000000) - 1;
	uint16_t overdrive_prescale = ((cfg->core_clock_frequency * 1) / 1000000) - 1;
	LOG_DBG("Desired prescale registers: normal 0x%04x, overdrive 0x%04x (peripheral core clock %dMHz)",
		normal_prescale, overdrive_prescale, cfg->core_clock_frequency / 1000000);
	*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_LOW)) = normal_prescale & 0xff;
	*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_HIGH)) = (normal_prescale >> 8) & 0xff;
	*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_LOW)) = overdrive_prescale & 0xff;
	*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_HIGH)) = (overdrive_prescale >> 8) & 0xff;
	if ((*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_LOW)) == (normal_prescale & 0xff)) &&
		(*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_NORMAL_HIGH)) == ((normal_prescale >> 8) & 0xff)) &&
		(*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_LOW)) == (overdrive_prescale & 0xff)) &&
		(*((volatile uint8_t *)(cfg->base_address + OPENCORES_1WIRE_MASTER_CLK_DIV_OVERDR_HIGH)) == ((overdrive_prescale >> 8) & 0xff))) {
		LOG_DBG("w1-opencores initialized, with %d slave devices", cfg->master_config.slave_count);
		return 0;
	}

	return -1;
}

static const struct w1_driver_api w1_opencores_master_driver_api = {
	.reset_bus = w1_opencores_master_reset_bus,
	.read_bit = w1_opencores_master_read_bit,
	.write_bit = w1_opencores_master_write_bit,
	.read_byte = w1_opencores_master_read_byte,
	.write_byte = w1_opencores_master_write_byte,
	.configure = w1_opencores_master_configure,
};

#define W1_ZEPHYR_OPENCORES_MASTER_INIT(inst)                                                           \
	static const struct w1_opencores_master_config w1_opencores_master_cfg_##inst = {               \
		.master_config.slave_count = W1_INST_SLAVE_COUNT(inst),                                 \
		.base_address = DT_INST_REG_ADDR(inst),                                                 \
		.core_clock_frequency = DT_INST_PROP(inst, opencores_ip_clock_frequency)};              \
	static struct w1_opencores_master_data w1_opencores_master_data_##inst = {};                    \
	DEVICE_DT_INST_DEFINE(inst, &w1_opencores_master_init, NULL, &w1_opencores_master_data_##inst,  \
			      &w1_opencores_master_cfg_##inst, POST_KERNEL, CONFIG_W1_INIT_PRIORITY,    \
			      &w1_opencores_master_driver_api);

DT_INST_FOREACH_STATUS_OKAY(W1_ZEPHYR_OPENCORES_MASTER_INIT)

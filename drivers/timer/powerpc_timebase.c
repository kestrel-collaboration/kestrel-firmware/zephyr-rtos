/*
 * Copyright (c) 2019 Michael Neuling <mikey@neuling.org>
 * Copyright (c) 2020-2022 Raptor Engineering, LLC <sales@raptorengineering.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/arch/cpu.h>
#include <zephyr/device.h>
#include <zephyr/irq.h>
#include <zephyr/drivers/timer/system_timer.h>

static uint64_t powerpc_decrementer_cycles_per_tick;

#ifndef CONFIG_OPENPOWER_LITEX_IRQ
/* FIXME: we only have 1 interrupt for now */
#define TIMER_IRQ_NUM 0
#endif

static inline void mtdec(uint64_t val)
{
	__asm__ volatile("mtdec %0" : : "r" (val) : "memory");
}

void powerpc_timebase_irq_handler(void *device)
{
	ARG_UNUSED(device);

	int key = irq_lock();

	mtdec(powerpc_decrementer_cycles_per_tick);
	sys_clock_announce(1); /* this does the scheduling */

	irq_unlock(key);
}

/* tickless kernel is not supported */
uint32_t sys_clock_elapsed(void)
{
	return 0;
}

uint32_t sys_clock_cycle_get_32(void)
{
	// Read timebase register from TB SPR (#268)
	uint64_t ppc64_timebase;
	__asm__ volatile ("mfspr %0, 268" : "=r" (ppc64_timebase));

	return ppc64_timebase;
}

uint64_t sys_clock_cycle_get_64(void)
{
	// Read timebase register from TB SPR (#268)
	uint64_t ppc64_timebase;
	__asm__ volatile ("mfspr %0, 268" : "=r" (ppc64_timebase));

	return ppc64_timebase;
}

int sys_clock_driver_init(const struct device *device)
{
	ARG_UNUSED(device);

	powerpc_decrementer_cycles_per_tick = sys_clock_hw_cycles_per_sec() / CONFIG_SYS_CLOCK_TICKS_PER_SEC;

#ifndef CONFIG_OPENPOWER_LITEX_IRQ
	// When the XICS controller driver is enabled, the DEC interrupt is directly wired
	// to powerpc_timebase_irq_handler(), bypassing the IRQ tables.
	// This is done to avoid conflicts between the "virtual" DEC interrupt number
	// and the physical interrupt numbers wired to various peripherals.
	IRQ_CONNECT(TIMER_IRQ_NUM, 0, powerpc_timebase_irq_handler, NULL, 0);
	irq_enable(TIMER_IRQ_NUM);
#endif

	mtdec(powerpc_decrementer_cycles_per_tick);

	return 0;
}

SYS_INIT(sys_clock_driver_init, PRE_KERNEL_2,
	 CONFIG_SYSTEM_CLOCK_INIT_PRIORITY);
